module Scraper
  require 'pathname'


  # this method just automatically returns all scraper classes.
  def self.providers
    tire_providers = ::Scraper::Providers::Tires.constants.map do |constant|
      "Scraper::Providers::Tires::#{constant.to_s}".constantize
    end

    rims_providers = ::Scraper::Providers::Rims.constants.map do |constant|
      "Scraper::Providers::Rims::#{constant.to_s}".constantize
    end

    batteries_providers = ::Scraper::Providers::Batteries.constants.map do |constant|
      "Scraper::Providers::Batteries::#{constant.to_s}".constantize
    end
    providers = tire_providers + rims_providers + batteries_providers
    providers
  end


  def self.provider(code)
    ::Scraper.providers.select { |s| s.info[:code] === code }.first
  end


  def self.run(debug: false, type: nil, scraper: nil, count: nil)
    ::Scraper.providers.each do |provider|
      scrap = true
      scrap = false if type.to_s    != '' && provider.info[:type].to_s != type.to_s
      scrap = false if scraper.to_s != '' && provider.info[:code].to_s != scraper.to_s

      if debug
        # -----------------------------------------
        provider.new(debug: true, count: count).run if scrap
        # -----------------------------------------
      else
        # -----------------------------------------
        begin
          provider.new(debug: false).run if scrap
        rescue ::Exception => ex
          # TODO: stop scraper from running
          # TODO: send email or something to notify there has been 500 in scraper
          ::ScraperError.create!(
            error_class: ex.class.to_s,
            provider:    provider.class.to_s,
            params:      {
              params:    ex.try(:params),
              message:   ex.try(:message),
              backtrace: ex.try(:backtrace)
            }
          )
        end
        # -----------------------------------------
      end

    end
  end


end

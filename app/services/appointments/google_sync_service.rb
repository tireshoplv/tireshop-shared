require "google/apis/calendar_v3"
require "googleauth"

module Appointments
  class GoogleSyncService
    APPLICATION_NAME = 'Tireshop.lv'.freeze
    CREDENTIALS_PATH = 'config/credentials.json'.freeze

    def initialize
      @service = build_service
    end

    def insert_event(appointment)
      begin
        event = build_event(appointment)
        @service.insert_event(calendar_id(appointment), event)
      rescue Exception => e
        puts 'Error caught while inserting event to google'
        puts e
      end
    end

    def delete_event(appointment)
      begin
        @service.delete_event(calendar_id(appointment), appointment.google_id)
      rescue Exception => e
        puts 'Error caught while deleting event from google'
        puts e
      end
    end

    def update_event(appointment)
      begin
        event = build_event(appointment)
        @service.update_event(calendar_id(appointment), appointment.google_id, event)
      rescue Exception => e
        puts 'Error caught while update event to google'
        puts e
      end
    end

    def build_service
      service = Google::Apis::CalendarV3::CalendarService.new
      service.client_options.application_name = APPLICATION_NAME
      service.authorization = authorize
      service
    end

    private

    def authorize
      authorizer = Google::Auth::ServiceAccountCredentials.make_creds(
        json_key_io: File.open(CREDENTIALS_PATH),
        scope: [
          'https://www.googleapis.com/auth/androidpublisher',
          Google::Apis::CalendarV3::AUTH_CALENDAR_EVENTS,
          Google::Apis::CalendarV3::AUTH_CALENDAR,
          Google::Apis::CalendarV3::AUTH_CALENDAR_SETTINGS_READONLY
        ]
      )

      authorizer
    end

    def calendar_id(appointment)
      Settings.appointments.send("calendar_id_#{appointment.service_box}".to_sym)
    end

    def build_event(appointment)
      Google::Apis::CalendarV3::Event.new ({
        id: appointment.google_id,
        summary: appointment.title,
        location: 'Tireshop',
        description: appointment.to_text,
        start: {
          date_time: appointment.start_time.to_datetime.rfc3339,
        },
        end: {
          date_time: appointment.end_time.to_datetime.rfc3339,
        },
      })
    end
  end
end

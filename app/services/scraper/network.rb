module Scraper
  class Network
    require 'net/ftp'


    def initialize
      @agent                          = ::Mechanize.new
      @agent.user_agent               = 'TireShopBot'
      @agent.agent.http.verify_mode   = OpenSSL::SSL::VERIFY_NONE
      @agent.pluggable_parser.default = ::Mechanize::Download
    end


    def get_ftp_file(host: nil, port: nil, username: nil, password: nil, path: nil, file: nil, fix_encoding: nil)
      local_path = ::File.join(::Dir.tmpdir, "tireshop_scraper_ftp_dl_tmp_file_#{::SecureRandom.uuid}.txt")
      ftp        = ::Net::FTP.new

      ftp.connect(host, port)
      ftp.login(username, password) if username && password
      ftp.chdir(path)
      ftp.binary = true
      ftp.passive = true
      ftp.getbinaryfile(file, local_path)
      ftp.close

      source = ::File.open(local_path).read

      if fix_encoding
        source = source.force_encoding(fix_encoding).encode!('UTF-8')
      end

      source
    end

    def get_image(url)
      Timeout::timeout(Settings['scraper']['max_wait_time_for_image_download_in_sec']) do

        ext  = url
        path = Rails.root.join('tmp', SecureRandom.hex + ext)

        # download image file
        # --------------------------------------
        begin
          @agent.get(url).save(path) # will raise error on failed download
        rescue Exception => ex
          return nil
        end
        # --------------------------------------

        # check if its an actual image (cos some sites return HTML)
        # --------------------------------------
        begin
          ::Magick::Image.read(path).first #  will raise error if image not image
        rescue Exception => ex
          return nil
        end
        # --------------------------------------

        path.to_s
      end rescue nil
    end


    def get_source(url, _username: nil, _password: nil)
      # if username && password
      #   @agent.add_auth('http://ordering.nevetas.lt', username, password)
      # end
      @agent.read_timeout = 10000
      enforce_encoding(@agent.get(url, read_timeout: 1000).body.to_s)
    end

    def get_latakko_products(product_type, token)
      @connection = Faraday.new
      response = @connection.get(::Scraper::VendorList.latakko.url) do |req|
        req.params = ::Scraper::Helpers::ParamsHelper.latakko_params("pcr")
        req.headers = { 'Authorization' => "bearer #{token}" }
      end
      JSON.parse(response.body)
    end


    private


    def enforce_encoding(str)
      str.force_encoding('UTF-8').encode("UTF-8", invalid: :replace, undef: :replace)
    end


  end
end

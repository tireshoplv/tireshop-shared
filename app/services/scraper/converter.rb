module Scraper
  class Converter

    def self.xml_to_json(xml)
      ::Hash.from_xml(xml)
    end

  end
end

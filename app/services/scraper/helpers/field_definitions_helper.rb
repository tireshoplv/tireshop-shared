# frozen_string_literal: true

module Scraper
  module Helpers
    class FieldDefinitionsHelper
      class << self
        VENDORS = [
          'tirespot'
        ].freeze

        def tirespot
          raise NotImplementedError unless VENDORS.include? __method__.to_s

          OpenStruct.new(
            {
              uid: 'kood',
              price: 'hulgihind',
              model: 'nimi',
              manufacturer: 'tootja',
              width: 'laius',
              height: 'korgus',
              diameter: 'diameeter',
              speed_rating: 'kiirusindeks',
              load_index: 'koormusindeks',
              season: 'hooaeg',
              qty: 'laos',
              vehicle_type: 'kategooria',
              fuel_efficiency: "",
              noise: "",
              wet_grip: "",
              specifications: ""
            }
          )
        end

        def tavas_riepas_lv
          OpenStruct.new(
            {
              uid: 'item_id',
              price: 'spec_price_incl_vat',
              model: 'model',
              manufacturer: 'make',
              width: 'width',
              height: 'height',
              diameter: 'radius',
              speed_rating: 'index',
              load_index: 'index',
              season: 'season',
              qty: 'stock',
              image_url: 'photos',
              vehicle_type: 'moto',
              fuel_efficiency: "",
              noise: "",
              wet_grip: "",
              specifications: ""
            }
          )
        end

        def tireshop_lv
          OpenStruct.new(
            {
              uid: "stock_id",
              price: "price",
              model: "model_name",
              manufacturer: "manufacturer_name",
              width: "width",
              height: "height",
              diameter: "diameter",
              speed_rating: "speed_index",
              load_index: "speed_index",
              season: "season",
              qty: "stock",
              image_url: "image_link",
              vehicle_type: "category",
              fuel_efficiency: "fuel_efficiency",
              noise: "noise",
              wet_grip: "wet_grip",
              specifications: "specifications"
            }
          )
        end
      end
    end
  end
end

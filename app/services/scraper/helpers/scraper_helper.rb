# frozen_string_literal: true

module Scraper
  module Helpers
    module ScraperHelper
      extend ActiveSupport::Concern

      AbstractedItem = Struct.new(:uid, :price, :model, :manufacturer, :url, :width, :height, :diameter,
        :speed_rating, :load_index, :season, :vehicle_type, :qty, :image_url, :fuel_efficiency, :noise,
        :wet_grip, :specifications, keyword_init: true)

      def vendor_data; end

      def result_klass; end

      def define_items(item)
        AbstractedItem.new(
          uid: item[@field_definitions.uid],
          price: item[@field_definitions.price],
          model: item[@field_definitions.model],
          manufacturer: item[@field_definitions.manufacturer],
          url: item[@field_definitions.url],
          width: item[@field_definitions.width],
          height: item[@field_definitions.height],
          diameter: item[@field_definitions.diameter],
          speed_rating: item[@field_definitions.speed_rating],
          load_index: item[@field_definitions.load_index],
          season: item[@field_definitions.season],
          vehicle_type: item[@field_definitions.vehicle_type],
          qty: item[@field_definitions.qty],
          image_url: item[@field_definitions.image_url],
          fuel_efficiency: item[@field_definitions.fuel_efficiency],
          noise: item[@field_definitions.noise],
          wet_grip: item[@field_definitions.wet_grip],
          specifications: item[@field_definitions.specifications]
        )
      end

      def prepare_product_from_raw_data(item, _items = [], provider_code, provider_type)
        # the logic should not be touched, field definitions helper should be used to set up field names for AbstractedItem struct.
        uid = item.uid.to_s

        if uid.blank?
          raise(
            ::Scraper::Errors::ScraperItemUidNotFoundError.new(
              params: { item: item }
            )
          )
        end

        # price
        
        price = [
          begin
            BigDecimal(item.price.to_s)
          rescue StandardError
            nil
          end,
          begin
            BigDecimal(item.price.to_s)
          rescue StandardError
            nil
          end
        ].reject(&:blank?).min
        return nil if price.nil?

        # find or initialize product
        product = ::Product
          .where(
            uid: uid,
            scraper: provider_code,
            product_type: provider_type
          ).first_or_initialize(
            uid: uid,
            scraper: provider_code,
            product_type: provider_type
          )

        # if product has no stock and not present in db for updating, we should skip this object for increased performance

        return nil if product.id.blank? && item.qty.to_i.zero?

        # tire fields
        if product.id.blank?

          # prefiller class to modularize this part. batteries and wheels will have their own. makes this method thinner.
          # by skipping attirbutes assigment to existing objects we can save loads of times to ensure fast stock update

          product = Scraper::Managers::TireManager.prefill_new_record(product, item)
        end

        return nil if product.blank?

        return nil if item.manufacturer.to_s.blank?

        product.original_image_url = item.image_url if item.image_url
        product.manufacturer_id = validate_manufacturer(
          item.manufacturer.to_s.downcase.to_s.strip,
          item
        ).try(:id)
        return nil if product.manufacturer_id.nil?

        product_vendor_params = {
          vendor_id: @vendor.id,
          price: price,
          qty: 0,
          qty_hour: 0,
          qty_days: 0,
          qty_weeks: item.qty
        }

        if product.id.nil?
          product.product_vendors_attributes = { '0' => product_vendor_params }
        else
          product.product_vendors_attributes = if (product_vendor = product.product_vendors.where(vendor_id: @vendor.id).first)
                                                 { '0' => product_vendor_params.merge(id: product_vendor.id) }
                                               else
                                                 { '0' => product_vendor_params }
                                               end
        end

        # raw data - comment in every case for now, since we probably have no use case for this atm.
        # --------------------------------------------------
        # if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item.to_h
        #   product.scraper_inputs_attributes = {
        #     '0' => { raw_data: item.to_h }
        #   }
        # end

        # add existing or initialized object to queue for bulk insert/upsert
        @items_to_update << product if product.id.present? && product.valid?
        @items_to_create << product if product.id.blank? && product.valid?
        product
      end

      def bulk_create_items(payload)
        time = Time.zone.now # neccessary precausious way to ensure that insert_all does not break
        payload.map do |obj|
          obj.created_at = time
          obj.hash_id = ::SecureRandom.uuid
          # ^we generate hash id here so that we wont have to run validator on insert/upsert
        end
        products = Product.import! payload, recursive: true # runs insert all with recursive association bulk insert.

        version_payload = payload.map do |product|
          {
            event: "admin_create",
            model: Product.to_s, 
            model_id: product.id,
            editor_id: 1
          }
        end
        Version.import! version_payload

        ScraperJobs::AwsImageExportJob.perform_async(products.ids)
        ::CreateProductPriceJob.perform_async(products.ids)

        products
      end

      def bulk_update_items(payload)
        ProductVendor.import! payload.map(&:product_vendors).flatten, # let's get rid of arrays within arrays, shall we?
          on_duplicate_key_update: %i[price qty qty_hour qty_days qty_weeks] # bulk upserts selected columns.

        ProductVendorPrice.import! payload.map(&:prices).flatten,
          on_duplicate_key_update: %i[price]

        ProductPrice.import! payload.map(&:product_prices).flatten,
          on_duplicate_key_update: %i[original_price price]
      end

      def validate_vendor(name, code, item)
        @vendor ||= ::Vendor.find_by(name: name)

        unless @vendor
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: code, raw_data: item }
            )
          )
        end

        @vendor
      end
    end
  end
end

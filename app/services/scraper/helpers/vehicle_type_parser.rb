module Scraper
  module Helpers
    class VehicleTypeParser 

      class << self 

        def parse_vehicle_type(value)
          Scraper::Maps::USAGE_CODES_MAP[value.downcase]
        end
      end
    end
  end
end
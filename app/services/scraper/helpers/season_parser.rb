module Scraper 
  module Helpers 
    class SeasonParser
      SEASON_CODES_MAP = {
        'sum' => Product::SUMMER,
        'estive' => Product::SUMMER,
        's' => Product::SUMMER,
        'v' => Product::SUMMER,
        'summer' => Product::SUMMER,
        'vasaras' => Product::SUMMER,
        'vasara' => Product::SUMMER,
        'vasaras riepas' => Product::SUMMER,
        'suverehv'       => Product::SUMMER,
        'vasarinė'       => Product::SUMMER,
        'žieminė'        => Product::WINTER,
        'žieminės'       => Product::WINTER,
        'win'            => Product::WINTER,
        'studded'        => Product::WINTER,
        'invernali'      => Product::WINTER,
        'naast'          => Product::WINTER,
        'w'              => Product::WINTER,
        'winter'         => Product::WINTER,
        'ziemas'         => Product::WINTER,
        'ziemas riepas'  => Product::WINTER,
        'žiema'          => Product::WINTER,
        'z'              => Product::WINTER,
        'lamelrehv'      => Product::WINTER,
        'lamellrehv'      => Product::WINTER,
        'naastrehv'      => Product::WINTER,
        'all season'     => Product::MS,
        'm+s'            => Product::MS,
        'universālās'    => Product::MS,
        'all season'     => Product::MS,
      }

      class << self
        def parse_season(value)
          SEASON_CODES_MAP[value.downcase]
        end
      end
    end
  end
end

module Scraper
  module Helpers
    class ParamsHelper

      LATAKKO_PARAMS = {
        pcr: {
          OnlyStockItems: true,
          OnlyLocalStockItems: true,
          IncludeCarTyres: true,
          IncludeEarthmoverTyres: false,
          IncludeSteelRims: false,
          IncludeAccessories: false,
          IncludeMotorcycleTyres: false,
          IncludeTruckTyres: false,
          IncludeAlloyRims: false,
          IncludeBatteries: false,
        }
      }
      class << self
        def latakko_params(product_type)
          LATAKKO_PARAMS[product_type.to_sym]
        end
      end
    end
  end
end

#9869
module Scraper 
  module Helpers 
    class AwsHelper
      class << self

        def parse_and_export_image(product)
          unless Rails.env.development? && (Settings['scraper']['add_images'])
            begin
              image = ::Scraper::Network.new.get_image(product.original_image_url)
              url = AwsService::S3.upload(image)
              product.aws_image_url = url
              print product.aws_image_url
              product
            rescue StandardError => e
              return nil
            end
          end
        end
      end
    end
  end
end



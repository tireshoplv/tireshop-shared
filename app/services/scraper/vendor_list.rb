# frozen_string_literal: true
module Scraper
  class VendorList
    class << self
      Result = Struct.new(:url, :host, :port, :path, :file, :username, :password, :fix_encoding,
        keyword_init: true)

      def method_missing(method, *args, &block)
        "Method '#{method}' is not implemented in Vendor List,"\
        " please, add vendor import parameters as a seperate method with corresponsing args"
      end
        
      def ad_baltic_lt
        Result.new(
          host: 'padangos.adbaltic.lt',
          port: 21,
          path: '/',
          file: 'tyres.xml',
          username: '40203159839@padangos.adbaltic.lt',
          password: '0yNn7nes',
          fix_encoding: 'UTF-16LE',
        )
      end

      def remik_lv
        Result.new(
          url: "https://www.remik.lv/cron/xml_generation.php?cid=48&token=9d1436e44b5152ac0325612e4b4b8d82"
        )
      end

      def handlopex_lt
        Result.new(
          host: "b2b.handlopex.lt",
          port: 21,
          path: "/",
          file: "xmlpricelist.xml",
          username: "pricelist@b2b.handlopex.lt",
          password: "1pXQB6te",
        )
      end

      def an_riepas_lt
        Result.new(
          url: "https://api.anriepas.lv/ware_xml.php?id=8&key=72781e0d1b2332415a056a9125526621cf34c9e6"
        )
      end

      def tavas_riepas_lv
        Result.new(
          url: 'https://www.tavasriepas.lv/b2b_export.xml?hash=668cc3fa9344fff035beefcac26f35ed'
        )
      end

      def atlas_riepas_lv
        Result.new(
          url: "http://www.atlasriepas.lv/atlas_a4_2018.xml"
        )
      end

      def riepu_centrs_lv
        Result.new(
          url: "http://www.riepucentrs.lv/export/tyreshop.php"
        )
      end

      def juta_lt
        Result.new(
          url: "http://juta.tsm-online.lt/xml_jt.php?dbid=JUTA&username=xml.tireshop&password=zRXLY1&dbid=JUTA"
        )
      end

      def veemann
        Result.new(
          url: "https://www.vannetukku.fi/wholesale_wheels.php?id=9dpUsgkr9duLE5vV"
        )
      end

      def vannetukku_fi
        Result.new(
          url: "https://www.vannetukku.fi/wholesale_wheels.php?id=9dpUsgkr9duLE5vV"
        )
      end

      def auto_riepu_nams
        Result.new(
          url: "https://www.riepas.lv/autoriepunams_xml.php?xml_key=rubbe"
        )
      end

      def tirespot
        Result.new(
          url: 'https://www.tirespot.ee/module/shopExport/tirespot/xmlExport.php'
        )
      end

      def balsana
        Result.new(
          url: 'http://support.balsana.lt/qty.xml'
        )
      end

      def autostarts 
        Result.new(
         url: 'http://bizness.autostarts.lv/xml/tireshop.lv-34674.xml'
        )
      end

      def riepas_com
        Result.new(
        url: 'http://gt.burti.lv/WebGTItems.hal?comp=2&customer=3712643&password=ytr876'
        )
      end
      
      def tyrelia_tyres
        Result.new(
          url: 'https://www.vannetukku.fi/wholesale_tyres.php?id=9dpUsgkr9duLE5vV'
        )
      end

      def tyreworld
        Result.new(
          ftp: 'tyretyre.de',
          path: '/download/stock',
          file: 'stock_tw_tshopsia.csv',
          username: 'TireshopSIA',
          password: 'zhgS3pBpDSjTKAyT',
        )
      end

      def felgeo 
        Result.new(
          'http://www.gl-traders1.nazwa.pl/stocks/felgeostocks.csv'
        )
      end

      def rota_wheels
        Result.new(
          url: 'http://trade.rotawheelsuk.com/CurrentStockProducts/rota.csv'
        )
      end

      def sklepopon
        Result.new(
          ftp: 'ftp.enisb2b.com',
          path: 'stock/',
        )
      end

      def wheel_trade
        Result.new(
          url: 'https://b2b.wheeltrade.pl/en/xmlapi/7/2/utf8/e8812d0b-7a09-46ae-b62c-6999b61e336e'
        )
      end
      
      def diski3_lv
        Result.new(
          url: 'http://www.wheels2020.com/files/rims.xml'
        )
      end

      def tuxauto
        Result.new(
          ftp: 'tireshop.lv',
          path: '/',
          file: 'stock.csv',
          username: 'tuxauto',
          password: 'ACFcc#ccf78ba88%d7113c2#df9%cCC5841#Fd5Eef%b3e27#a1ebbe8cd88701d46448bfab',
        )
      end

      def latakko
        Result.new(
          url: "https://api.latakko.eu/api/Articles"
        )
      end

      def tireshop_lv
        if Rails.env.development?
          Result.new(
            url: "http://localhost:3002/api/products"
          )
        else
          Result.new(
            url: "http://adm.tireshop.lv/api/products"
          )
        end
      end
      
      private
    end
  end
end

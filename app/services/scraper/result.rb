##
# This class is used as a result for every aspect of the scraper.
# Every single method returns this as response. We do this, so we
# have standardized response.
#
# Successful response example:
#   ::Scraper::Result.new(success: true, result: '')
#
# Unsuccessful response example
#   ::Scraper::Result.new(
#     success:   false,                       # boolean          (required)
#     code:      'shot_code_like_error_name', # code             (required)
#     uid:       '',                          # record uid       (optional)
#     provider:  '',                          # record provider  (optional)
#     message:   'error message',             # error message    (optional)
#     backtrace:  nil,                        # error backtrace  (required)
#     params:    {}                           # extra parameters (optional)
#   )
#
# Automatically log unsuccessful response example
#   ::Scraper::Result.new(
#     success:   false,                       # boolean          (required)
#     code:      'shot_code_like_error_name', # code             (required)
#     uid:       '',                          # record uid       (optional)
#     provider:  '',                          # record provider  (optional)
#     message:   'error message',             # error message    (optional)
#     backtrace: nil,                         # error backtrace  (required)
#     params:    {}                           # extra parameters (optional)
#   ).log_error
#
module Scraper
  class Result


    attr_accessor :success, :result,
                  :code, :message, :backtrace, :params, :uid, :provider


    def initialize(success: true, result: nil, code: nil, message: nil, backtrace: nil, params: {}, uid: nil, provider: nil)
      @uid       = uid
      @provider  = provider
      @success   = success
      @result    = result
      @code      = code
      @params    = params
      @message   = message
      @backtrace = backtrace
    end


    def log_error
      unless @success
        errors = ::JSON.parse(::File.read('errors.json', encoding: 'UTF-8')) rescue []

        if @uid.nil?
          errors << ::JSON.parse(self.to_json)
        else
          errors << ::JSON.parse(self.to_json) unless errors.select { |er| er['uid'] == @uid }.any?
        end

        ::File.write('errors.json', ::JSON.pretty_generate(errors), encoding: 'UTF-8')
      end

      self # return class, as this class IS used as a result object
    end


    def as_json
      {
        success:   @success,
        code:      @code,
        message:   @message,
        backtrace: @backtrace,
        params:    @params,
        uid:       @uid,
        provider:  @provider
      }
    end


    def to_json
      self.as_json.to_json
    end


  end
end

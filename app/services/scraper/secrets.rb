module Scraper
  module Secrets

    def self.get(provider, type, key)
      path  = [:scraper, provider.to_sym, type.to_sym, key.to_sym]
      value = ::Rails.application.secrets.dig(*path)

      raise(
        Scraper::Errors::SecretsValueNotFoundError.new(
          params: { path: path.join('.') }
        )
      ) if value.nil?

      value
    end

  end
end

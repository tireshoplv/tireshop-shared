module Scraper
  module Managers
    module TireManager
      class << self
        def prefill_new_record(product, item)
          begin
            product.model    = item.model.to_s
            product.url      = nil
            product.width    = BigDecimal(item.width.to_s)
            product.height   = BigDecimal(item.height.to_s)
            product.diameter = BigDecimal(item.diameter.to_s)
            product.speed_rating = item.speed_rating.downcase if item.speed_rating
            product.load_index = item.load_index

            if item.vehicle_type
              product.vehicle_type = Scraper::Helpers::VehicleTypeParser.parse_vehicle_type(item.vehicle_type)
              product.vehicle_type = Product::CAR unless product.vehicle_type
            end

            return nil unless item.vehicle_type

            return nil unless item.season

            product.season = Scraper::Helpers::SeasonParser.parse_season(item.season)

            return nil unless product.season

            product.wet_breaking_efficiency = item.wet_grip.downcase if item.wet_grip
            product.noise_index = item.noise if item.noise
            product.fuel_efficiency = item.fuel_efficiency if item.fuel_efficiency
            product.specifications = [item.specifications] if item.specifications

            # product.run_flat = true if item['run-flat'].downcase === 'jā'

            # check most important fields and SKIP if they are missing
            unless [
              product.model.nil?,
              product.width.nil?,
              product.height.nil?,
              product.diameter.nil?,
              product.speed_rating.nil?
            ].include?(false)
              return nil
            end
          rescue => e
            nil
          end
          
          product
        end
      end
    end
  end
end
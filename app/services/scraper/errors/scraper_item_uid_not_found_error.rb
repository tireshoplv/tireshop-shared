module Scraper
  module Errors
    class ScraperItemUidNotFoundError < StandardError
      attr_reader :params

      def initialize(msg: 'Scraper item invalid format', params: {})
        @params = params
        super(msg)
      end

    end
  end
end

module Scraper
  module Errors
    class ScraperItemParseFailureError < StandardError
      attr_reader :params

      def initialize(msg: 'Failed to parse scraper item', params: {})
        @params = params
        super(msg)
      end

    end
  end
end

module Scraper
  module Errors
    class SecretsValueNotFoundError < StandardError
      attr_reader :params

      def initialize(msg: 'Secrets value not found', params: {})
        @params = params
        super(msg)
      end

    end
  end
end

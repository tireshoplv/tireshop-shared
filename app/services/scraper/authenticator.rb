module Scraper
  class Authenticator
    class << self 
      Token = Struct.new(:token, :errors, keyword_init: true)
      def latakko
        auth_url = 'https://api.latakko.eu/Token'
        token = fetcher.post(auth_url) do |conn|
          conn.headers['Content-Type'] = 'application/x-www-form-urlencoded'
          conn.body = { 'grant_type' => 'password', 'username' => 'apiTireshop', 'password' => 'b8m2Pcrtya0WMW' }
        end

        Token.new(token: parse_token(token.body), errors: nil)
      end

      private 

      def fetcher
        Faraday.new
      end

      def parse_token(body)
        OpenStruct.new(JSON.parse(body)).access_token
      end
    end
  end
end

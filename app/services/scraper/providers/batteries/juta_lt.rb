module Scraper
  module Providers
    module Batteries
      class JutaLt
        PROVIDER_CODE = 'juta_lt'.freeze
        PROVIDER_TYPE = 'battery'
        NAME          = 'juta.lt'.freeze
        URL           = 'https://www.juta.lt'.freeze # TODO:  change this to correct url

        PRODUCT_TYPES = {
          battery: %w[
            10000002342
            10000002343
            10000002347
            10000002402
            10000002403
            10000003122
           ]
        }


        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================


        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================


        # ==============================================================================
        def run
          puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}'" if @debug

          # get & parse data
          # -----------------------------------------------------
          url    = ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :url)
          source = ::Scraper::Network.new.get_source(url)
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('data', 'products', 'product')
          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          raise(
            ::Scraper::Errors::ScraperItemInvalidFormatError.new(
              params: {
                items: items,
                path:  'data.products.product'
              }
            )
          ) if items.nil?
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|

            if PRODUCT_TYPES[:battery].include?(item['groupid'].to_s)
              puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}" if @debug
              create_or_update_product_from_raw_data(item, items)
            end

          end
          # -----------------------------------------------------
        end
        # ==============================================================================


        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])
          if PRODUCT_TYPES[:battery].include?(item['groupid'].to_s)
            create_or_update_battery_from_raw_data(item, _items)
          else
            nil
          end
        end
        # ==============================================================================


        private


        # ==============================================================================
        def create_or_update_battery_from_raw_data(item, _items = [])

          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item['id'].to_s.strip

          raise(
            ::Scraper::Errors::ScraperItemUidNotFoundError.new(
              params: { item: item }
            )
          ) if uid.blank?
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = ::BigDecimal.new(
            item['price'].to_s.match(/[0-9,\.?,\,?]+/).to_a.last
          ) rescue nil
          return nil if price.nil?
          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                      .where(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
                      .first_or_initialize(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
          # -------------------------------------------------


          # Current (amperage)
          # -------------------------------------------------
          product.current = (
            item['params']['param'].select { |x| x['id'] == '10000000226' }.last['value'].to_i rescue nil
          )
          # -------------------------------------------------

          # name
          # -------------------------------------------------
          product.model = item['note2']
          # -------------------------------------------------

          # Voltage
          # -------------------------------------------------
          product.voltage = (
            item['params']['param'].select { |x| x['id'] == '10000000503' }.last['value'] rescue nil
          )
          # -------------------------------------------------

          # Capacity
          # -------------------------------------------------
          product.capacity = (
            item['params']['param'].select { |x| x['id'] == '10000000482' }.last['value'] rescue nil
          )
          # -------------------------------------------------

          # Height
          # -------------------------------------------------
          product.height = (
            item['params']['param'].select { |x| x['id'] == '10000000224' }.last['value'] rescue nil
          )
          # -------------------------------------------------

          # Length
          # -------------------------------------------------
          product.length = (
            item['params']['param'].select { |x| x['id'] == '10000000222' }.last['value'] rescue nil
          )
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.current.nil?,
            product.voltage.nil?,
            product.capacity.nil?
          ].include?(true)
            return nil
          end
          # -------------------------------------------------

          # image
          # -------------------------------------------------
          if Settings['scraper']['add_images']
            if product.new_record?
              if item['jpg1']
                image         = ::Scraper::Network.new.get_image(item['jpg1'])
                product.image = ::File.open(image) if image
              end
            end
          end
          # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          product.manufacturer_id = validate_manufacturer(
            item['producer'].to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?
          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price:     price,
            qty:       item['qty'].to_s.match(/[0-9]+/).to_a.last,
            qty_hour:  0,
            qty_days:  0,
            qty_weeks: 0
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              product.product_vendors_attributes = { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              product.product_vendors_attributes = { '0' => product_vendor_params }
            end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          product.scraper_inputs_attributes = {
            '0' => { raw_data: item }
          } if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item
          # --------------------------------------------------

          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

        end
        # ==============================================================================



        private


        def validate_vendor(item)
          vendor = ::Vendor.find_by_alias(PROVIDER_CODE)

          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
            )
          ) unless vendor

          vendor
        end


        def validate_manufacturer(manufacturer_alias, item)
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'manufacturer not found', raw_data: item }
            )
          ) if manufacturer_alias.to_s.blank?

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(            product_type: PROVIDER_TYPE, name: manufacturer_alias )
              .first_or_create!( product_type: PROVIDER_TYPE, name: manufacturer_alias )
          end

          manufacturer
        end


      end
    end
  end
end




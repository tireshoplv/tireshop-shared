module Scraper
  module Providers
    module Rims
      class VannetukkuFi
        PROVIDER_CODE = 'vannetukku_fi'.freeze
        PROVIDER_TYPE = 'rim'
        NAME          = 'Vannetukku.fi'.freeze
        URL           = 'https://www.vannetukku.fi'.freeze


        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================


        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================


        # ==============================================================================
        def run
          puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}'" if @debug

          # get & parse data
          # -----------------------------------------------------
          url    = ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :url)
          source = ::Scraper::Network.new.get_source(url)
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('Tyres', 'Item')
          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          raise(
            ::Scraper::Errors::ScraperItemInvalidFormatError.new(
              params: {
                items: items,
                path:  'Tyres.Item'
              }
            )
          ) if items.nil?
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}" if @debug
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================


        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])

          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item['Code'].to_s

          raise(
            ::Scraper::Errors::ScraperItemUidNotFoundError.new(
              params: { item: item }
            )
          ) if uid.blank?
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = ::BigDecimal.new(
            item['Wholesale_price_eur'].to_s.match(/[0-9,\.?,\,?]+/).to_a.last
          ) rescue nil
          return nil if price.nil?
          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                      .where(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
                      .first_or_initialize(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
          # -------------------------------------------------

          # URL
          # -------------------------------------------------
          product.url = "https://www.vannetukku.fi/bearpaw-p-#{product.uid}.html"
          # -------------------------------------------------

          # ean
          # -------------------------------------------------
          product.ean = item['EAN'].to_s.match(/[0-9]{13}/).to_a.last
          # -------------------------------------------------

          # bolts
          # -------------------------------------------------
          product.bolts_included = true  if item['Bolts_included'].to_s == 'yes'
          product.bolts_included = false if item['Bolts_included'].to_s == 'no'
          # -------------------------------------------------

          # season
          # -------------------------------------------------
          product.season = 'w' if item['Winter'].to_s == 'true'
          product.season = 's' if item['Winter'].to_s == 'false'
          # -------------------------------------------------

          # rim fields
          # -------------------------------------------------
          product.centre_bore           = ::BigDecimal.new(item['CB']) if item['CB']
          product.width                 = ::BigDecimal.new(item['Size'].split('x').first)
          product.model                 = item['Model'].to_s
          product.offset                = ::BigDecimal.new(item['ET'])
          product.diameter              = ::BigDecimal.new(item['Size'].split('x').last)
          product.description           = item['Description'].to_s
          product.freight_class         = ::BigDecimal.new(item['FreightClass'])
          product.pitch_circle_diameter = item['PCD']
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.width.nil?,
            product.offset.nil?,
            product.diameter.nil?,
            product.pitch_circle_diameter.nil?
          ].include?(true)
            return nil
          end
          # -------------------------------------------------

          # image
          # -------------------------------------------------
          if Settings['scraper']['add_images']
            if product.new_record?
              if item['Image_Url']
                image         = ::Scraper::Network.new.get_image(item['Image_Url'])
                product.image = ::File.open(image) if image
              end
            end
          end
          # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          product.manufacturer_id = validate_manufacturer(
            item['Brand'].to_s.downcase.to_s.strip.to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?
          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price:     price,
            qty:       0,
            qty_hour:  0,
            qty_days:  0,
            qty_weeks: item['Available_pcs'].to_s.match(/[0-9]+/).to_a.last,
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              product.product_vendors_attributes = { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              product.product_vendors_attributes = { '0' => product_vendor_params }
            end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          product.scraper_inputs_attributes = {
            '0' => { raw_data: item }
          } if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item
          # --------------------------------------------------

          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------


        end
        # ==============================================================================



        private


        def validate_vendor(item)
          vendor = ::Vendor.find_by_alias(PROVIDER_CODE)

          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
            )
          ) unless vendor

          vendor
        end


        def validate_manufacturer(manufacturer_alias, item)
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'manufacturer not found', raw_data: item }
            )
          ) if manufacturer_alias.to_s.blank?

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(            product_type: PROVIDER_TYPE, name: manufacturer_alias )
              .first_or_create!( product_type: PROVIDER_TYPE, name: manufacturer_alias )
          end

          manufacturer
        end


      end
    end
  end
end

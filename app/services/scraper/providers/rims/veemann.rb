module Scraper
  module Providers
    module Rims
      class Veemann
        require 'csv'

        PROVIDER_CODE = 'veemann'.freeze
        PROVIDER_TYPE = 'rim'
        NAME          = 'Veemann'.freeze
        URL           = 'Veemann'.freeze

        def initialize(debug: false, count: nil)
          @debug = debug
        end

        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end

        def run
          Rails.logger.debug "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}'" if @debug

          # get & parse data
          # -----------------------------------------------------
          url    = ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :url)
          json   = get_json(url)
          items  = json.map { |item| OpenStruct.new(item) }
          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          raise(
            ::Scraper::Errors::ScraperItemInvalidFormatError.new(
              params: {
                items: items,
                path: 'Tyres.Item'
              }
            )
          ) if items.nil?
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            Rails.logger.debug "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}" if @debug
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================

        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])
          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item.item_code.to_s

          raise(
            ::Scraper::Errors::ScraperItemUidNotFoundError.new(
              params: { item: item }
            )
          ) if uid.blank?
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = BigDecimal(
            item.rrp.to_s
          ) rescue nil
          return nil if price.nil?

          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                    .where(
                      uid: uid,
                      scraper: PROVIDER_CODE,
                      product_type: PROVIDER_TYPE
                    )
                    .first_or_initialize(
                      uid: uid,
                      scraper: PROVIDER_CODE,
                      product_type: PROVIDER_TYPE
                    )
          # -------------------------------------------------

          # URL
          # -------------------------------------------------
          product.url = nil
          # -------------------------------------------------

          # ean
          # -------------------------------------------------
          product.ean = nil
          # -------------------------------------------------

          # bolts
          # -------------------------------------------------
          product.bolts_included = false
          # -------------------------------------------------

          # season
          # -------------------------------------------------

          # -------------------------------------------------

          # rim fields
          # -------------------------------------------------
          product.centre_bore           = BigDecimal(item.centre_bore.to_s)
          product.width                 = BigDecimal(item.size.split("x").last)
          product.model                 = item.wheel_model_name
          product.offset                = BigDecimal(item.offest.gsub(/[^0-9,.]/, ""))
          product.diameter              = BigDecimal(item.size.split("x").first)
          product.description           = item.description.to_s
          product.freight_class         = nil
          product.pitch_circle_diameter = item.pcd
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.width.nil?,
            product.offset.nil?,
            product.diameter.nil?,
            product.pitch_circle_diameter.nil?
          ].include?(true)
            return nil
          end

          # -------------------------------------------------

          # image
          # -------------------------------------------------
          if Settings['scraper']['add_images']
            if product.new_record?
              if item.image
                image = ::Scraper::Network.new.get_image(item.image)
                url = AwsService::S3.upload(image)
                product.aws_image_url = url
              end
            end
          end

          # -------------------------------------------------
          # manufacturer
          # -------------------------------------------------
          product.manufacturer_id = validate_manufacturer(
            item.brand.to_s.downcase.to_s.strip.to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?

          # -------------------------------------------------
          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price: price,
            qty: 0,
            qty_hour: 0,
            qty_days: 0,
            qty_weeks: 4
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              product.product_vendors_attributes = { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              product.product_vendors_attributes = { '0' => product_vendor_params }
            end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          product.scraper_inputs_attributes = {
            '0' => { raw_data: item.to_h }
          } if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item.to_h
          # --------------------------------------------------

          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------
        end
        # ==============================================================================

        private

        def validate_vendor(item)
          vendor = ::Vendor.find_by(name: NAME)

          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
            )
          ) unless vendor

          vendor
        end

        def validate_manufacturer(manufacturer_alias, item)
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'manufacturer not found', raw_data: item }
            )
          ) if manufacturer_alias.to_s.blank?

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first_or_create(name: manufacturer_alias)

          if manufacturer.aliases.nil?
            manufacturer.aliases = [ManufacturerAlias.where(product_type: "tire",
                                                            name: manufacturer_alias.downcase).first_or_create]
          end
          manufacturer
        end

        def get_json(url)
          doc = open(url)
          records = CSV.foreach(doc, headers: true, :col_sep => ",", :header_converters => :symbol, :converters => :all)
          records.to_a.map { |row| row.to_hash }
        end
      end
    end
  end
end

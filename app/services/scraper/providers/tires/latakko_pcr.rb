# frozen_string_literal: true
require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class LatakkoPcr
        include ::Scraper::Helpers::ScraperHelper
        PROVIDER_CODE = 'latakko_pcr'.freeze
        PROVIDER_TYPE = 'tire'.freeze
        NAME          = 'Latakko Tires'.freeze
        URL           = 'https://i3.latakko.lv'.freeze
        PRODUCT_TYPE  = 'pcr'.freeze


        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================


        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================


        # ==============================================================================
        def run
          # get & parse data
          # -----------------------------------------------------
          token = ::Scraper::Authenticator.latakko.token
          source = ::Scraper::Network.new.get_latakko_products(PRODUCT_TYPE, token)
          json   = source
          json.map do |item|
            item.transform_keys! { |key| key.to_s.underscore }
          end
          items = json.map { |item| OpenStruct.new(item) }
          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          if items.nil?
            raise(
              ::Scraper::Errors::ScraperItemInvalidFormatError.new(
                params: {
                  items: items,
                  path:  'tyres.tyre'
                }
              )
            )
          end
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}" if @debug
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================


        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])
          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item.article_id

          if uid.blank?
            raise(
              ::Scraper::Errors::ScraperItemUidNotFoundError.new(
                params: { item: item }
              )
            )
          end
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = begin
                    ::BigDecimal.new(item.retail_price.to_s)
                  rescue
                    nil
                  end
          return nil if price.nil?

          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                    .where(
                        uid: uid,
                        scraper: PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
                    .first_or_initialize(
                        uid: uid,
                        scraper: PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
          # -------------------------------------------------

          # tire fields
          # -------------------------------------------------
          product.width  = ::BigDecimal.new(item.width.to_s) if item.width
          product.height = ::BigDecimal.new(item.aspect_ratio.to_s) if item.aspect_ratio
          product.diameter = ::BigDecimal.new(item.diameter.to_s) if item.diameter
          product.noise_index = item.noice_value if item.noice_value
          product.load_index = item.load_index.to_i if item.load_index
          if item.speed_index
            product.speed_rating = item.speed_index.downcase
          end
          if item.fuel_efficiency
            product.fuel_efficiency = item.fuel_efficiency.to_s.downcase
          end
          if item.wet_grip
            product.wet_breaking_efficiency = item.wet_grip.to_s.downcase
          end
          product.model = item.pattern_model_text.to_s
          # product.specification           = item['modifications']['mark'].to_s                       if item['modifications']['mark']
          # -------------------------------------------------
          
          # season
          # -------------------------------------------------
          product.season = 'w' if item.is_winter_approved.present?
          product.season = 's' if item.is_winter_approved.blank?
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.width.nil?,
            product.height.nil?,
            product.diameter.nil?,
            product.speed_rating.nil?
          ].include?(true)
            return nil
          end
          
          # -------------------------------------------------

          # image
          # -------------------------------------------------

          unless Rails.env.development?
            if Settings['scraper']['add_images']
              begin
                if item['photo'].present?
                  image = ::Scraper::Network.new.get_image(item['photo'])
                  url = AwsService::S3.upload(image)
                  product.aws_image_url = url
                end
              rescue StandardError => e
                return nil
              end
            end
          end
          # # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          product.manufacturer_id = validate_manufacturer(
            item.brand_name.to_s.downcase.to_s.strip,
            item.to_h
          ).try(:id)
          return nil if product.manufacturer_id.nil?

          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)
          
          product_vendor_params = {
            vendor_id: vendor.id,
            price: price,
            qty: item.quantity_available.to_i,
            qty_hour: 0,
            qty_days: 0,
            qty_weeks: 0
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            product.product_vendors_attributes = if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              { '0' => product_vendor_params }
                                                 end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          
          if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item.to_h
            product.scraper_inputs_attributes = {
              '0' => { raw_data: item.to_h }
            }
          end
          # --------------------------------------------------
          
          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

          product
        end
        # ==============================================================================



        private


        def validate_vendor(item)
          vendor = ::Vendor.find_by_alias(PROVIDER_CODE)

          unless vendor
            raise(
              ::Scraper::Errors::ScraperItemParseFailureError.new(
                params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
              )
            )
          end

          vendor
        end

        def validate_manufacturer(manufacturer_alias, item)
          if manufacturer_alias.to_s.blank?
            raise(
              ::Scraper::Errors::ScraperItemParseFailureError.new(
                params: { message: 'manufacturer not found', raw_data: item }
              )
            )
          end

          @manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first
          if @manufacturer.nil?
            @manufacturer = Manufacturer.create!(name: manufacturer_alias)
            @alias = ManufacturerAlias
              .where(product_type: PROVIDER_TYPE, name: manufacturer_alias)
              .first_or_create!(product_type: PROVIDER_TYPE, name: manufacturer_alias, manufacturer: @manufacturer)
            @manufacturer
          end

          @manufacturer
        end
      end
    end
  end
end

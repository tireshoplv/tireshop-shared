require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class RiepuZonaLv
        PROVIDER_CODE = 'riepu_zona_lv'.freeze
        PROVIDER_TYPE = 'tire'
        NAME          = 'RiepuZona.lv'.freeze
        URL           = 'https://riepuzona.lv'.freeze


        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================


        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================


        # ==============================================================================
        def run

          # get & parse data
          # -----------------------------------------------------
          url    = ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :url)
          source = ::Scraper::Network.new.get_source(url)
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('root', 'item')
          # -----------------------------------------------------


          # raise error if failed to parse data
          # -----------------------------------------------------
          raise(
            ::Scraper::Errors::ScraperItemInvalidFormatError.new(
              params: {
                items: items,
                path:  'root.item'
              }
            )
          ) if items.nil?
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}" if @debug
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================


        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])

          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item['code'].to_s

          raise(
            ::Scraper::Errors::ScraperItemUidNotFoundError.new(
              params: { item: item }
            )
          ) if uid.blank?
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = [
            (::BigDecimal.new( item['price']    ) rescue nil),
            (::BigDecimal.new( item['discount'] ) rescue nil)
          ].clean.min
          return nil if price.nil?
          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                      .where(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
                      .first_or_initialize(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
          # -------------------------------------------------

          # tire fields
          # -------------------------------------------------

          product.model    = item['name'].to_s
          product.url      = item['link'].to_s
          product.width    = ::BigDecimal.new(item['platums'].to_s)  if item['platums']
          product.height   = ::BigDecimal.new(item['augstums'].to_s) if item['augstums']
          product.diameter = ::BigDecimal.new(item['diametrs'].to_s) if item['diametrs']
          product.speed_rating = parse_speed_rating(item["name"].split.second).downcase
          product.load_index = parse_load_index(item["name"].split.second)

          # -------------------------------------------------

          # season
          # -------------------------------------------------
          product.season = 'ms' if item['sezona'] == 'Vissezonas'
          product.season = 'w'  if item['sezona'] == 'Ziemas'
          product.season = 's'  if item['sezona'] == 'Vasaras'
          # -------------------------------------------------

          # run flat
          # -------------------------------------------------
          # product.run_flat = true if item['run-flat'].downcase === 'jā'
          # -------------------------------------------------

          # studs
          # -------------------------------------------------
          product.studs = true if item['radzes'].to_s.downcase === 'ir'
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.width.nil?,
            product.height.nil?,
            product.diameter.nil?,
            product.speed_rating.nil?
          ].include?(true)
            return nil
          end
          # -------------------------------------------------

          # image
          # -------------------------------------------------
          unless Rails.env.development?
            if Settings['scraper']['add_images']
              begin
                if item["image"].present?
                  image = ::Scraper::Network.new.get_image("http://#{item["image"]}")
                  url = AwsService::S3.upload(image)
                  product.aws_image_url = url
                end
              rescue StandardError => e
                return nil
              end
            end
          end
          # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          return nil if item['razotajs'].to_s.blank?

          product.manufacturer_id = validate_manufacturer(
            item['razotajs'].to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?
          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price:     price,
            qty:       item['stock_amount'].to_i,
            qty_hour:  0,
            qty_days:  0,
            qty_weeks: 0,
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              product.product_vendors_attributes = { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              product.product_vendors_attributes = { '0' => product_vendor_params }
            end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          product.scraper_inputs_attributes = {
            '0' => { raw_data: item }
          } if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item
          # --------------------------------------------------

          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

          product
        end
        # ==============================================================================



        private

        def parse_speed_rating(string)
          string.split(/(?<=\d)(?=[A-Za-z])/).last
        end

        def parse_load_index(string)
          string.split(/(?<=\d)(?=[A-Za-z])/).first
        end

        def validate_vendor(item)
          vendor = ::Vendor.find_by_alias(PROVIDER_CODE)

          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
            )
          ) unless vendor

          vendor
        end


        def validate_manufacturer(manufacturer_alias, item)
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'manufacturer not found', raw_data: item }
            )
          ) if manufacturer_alias.to_s.blank?

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(            product_type: PROVIDER_TYPE, name: manufacturer_alias )
              .first_or_create!( product_type: PROVIDER_TYPE, name: manufacturer_alias )
          end

          manufacturer
        end

      end
    end
  end
end

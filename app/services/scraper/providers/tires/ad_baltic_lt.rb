require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class AdBalticLt
        PROVIDER_CODE = 'ad_baltic_lt'.freeze
        PROVIDER_TYPE = 'tire'
        NAME          = 'AdBaltic.lt'.freeze
        URL           = 'https://adbaltic.lt'.freeze


        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================


        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================


        # ==============================================================================
        def run
          puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}'" if @debug

          # get & parse data
          # -----------------------------------------------------
          source = ::Scraper::Network.new.get_ftp_file(
            host:         ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :host),
            port:         ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :port),
            path:         ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :path),
            file:         ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :file),
            username:     ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :username),
            password:     ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :password),
            fix_encoding: ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :fix_encoding),
          )
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('TYRES', 'Product')
          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          raise(
            ::Scraper::Errors::ScraperItemInvalidFormatError.new(
              params: {
                items: items,
                path:  'TYRES.Product'
              }
            )
          ) if items.nil?
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}" if @debug
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------

        end
        # ==============================================================================


        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])

          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item['ProductID'].to_s

          raise(
            ::Scraper::Errors::ScraperItemUidNotFoundError.new(
              params: { item: item }
            )
          ) if uid.blank?
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = ::BigDecimal.new(item['Price']) rescue nil
          return nil if price.nil?
          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                      .where(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
                      .first_or_initialize(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
          # -------------------------------------------------

          # season
          # -------------------------------------------------
          product.season = 'ms'
          product.season = 'w' if item['Season'].to_s.downcase.include?('ziemas')
          product.season = 's' if item['Season'].to_s.downcase.include?('vasaras')
          # -------------------------------------------------

          # studs
          # -------------------------------------------------
          # product.studs = false if item['Stud'] == 'Radžojamas'
          product.studs = false if item['Stud'] == 'Bez radzēm'
          product.studs = true  if item['Stud'] == 'Ar radzēm'
          # -------------------------------------------------

          # radius & diameter
          # -------------------------------------------------
          if item['MeasuringRim']
            product.radius   = BigDecimal.new(/[0-9]+/.match(item['MeasuringRim']).to_a.first)
            product.diameter = BigDecimal.new(product.radius * 2)
          end
          # -------------------------------------------------

          # tire fields
          # -------------------------------------------------
          product.model                   = item['Model'].to_s
          product.width                   = ::BigDecimal.new(item['Width'])                      if item['Width']
          product.height                  = ::BigDecimal.new(item['Height'])                     if item['Height']
          product.length                  = ::BigDecimal.new(item['ProductLength'])              if item['ProductLength']
          product.noise_index             = /[0-9]+/.match(item['NoiseDB']).to_a.first.to_i      if item['NoiseDB']
          product.description             = item['Description']                                  if item['Description']
          product.speed_rating            = item['SpeedIndex'].to_s.downcase                     if item['SpeedIndex']
          product.fuel_efficiency         = item['Fuel'].to_s.downcase                           if item['Fuel']
          product.wet_breaking_efficiency = item['Grid'].to_s.downcase                           if item['Grid']
          # product.specification           = item['Mark'].to_s                                    if item['Mark']
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.width.nil?,
            product.height.nil?,
            product.diameter.nil?,
            product.speed_rating.nil?
          ].include?(true)
            return nil
          end
          # -------------------------------------------------

          # image
          # -------------------------------------------------
          if Settings['scraper']['add_images']
            if product.new_record?
              if item['Picture']
                image         = ::Scraper::Network.new.get_image(item['Picture'])
                product.image = ::File.open(image) if image
              end
            end
          end
          # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          product.manufacturer_id = validate_manufacturer(
            item['Manufacturer'].to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?
          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          qty = item['RemainingQuantity'].to_i
          qty = 0 if qty < 0

          product_vendor_params = {
            vendor_id: vendor.id,
            price:     price,
            qty:       qty,
            qty_hour:  0,
            qty_days:  0,
            qty_weeks: 0,
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              product.product_vendors_attributes = { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              product.product_vendors_attributes = { '0' => product_vendor_params }
            end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          product.scraper_inputs_attributes = {
            '0' => { raw_data: item }
          } if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item
          # --------------------------------------------------

          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

          product
        end
        # ==============================================================================



        private


        def validate_vendor(item)
          vendor = ::Vendor.find_by_alias(PROVIDER_CODE)

          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
            )
          ) unless vendor

          vendor
        end


        def validate_manufacturer(manufacturer_alias, item)
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'manufacturer not found', raw_data: item }
            )
          ) if manufacturer_alias.to_s.blank?

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(            product_type: PROVIDER_TYPE, name: manufacturer_alias )
              .first_or_create!( product_type: PROVIDER_TYPE, name: manufacturer_alias )
          end

          manufacturer
        end

      end
    end
  end
end

# frozen_string_literal: true

require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class JuvaLv
        PROVIDER_CODE = 'juva_lv'
        PROVIDER_TYPE = 'tire'
        NAME          = 'Juva.lv'
        URL           = 'https://juva.lv'

        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================

        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================

        # ==============================================================================
        def run
          # get & parse data
          # -----------------------------------------------------
          url    = 'https://juva.lv/user_prices.xml?member=info@amiriepas.lv&password=435fa028c1578324e5ef1a4030350882'
          source = ::Scraper::Network.new.get_source(url)
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('root', 'item').map { |item| item.transform_keys(&:downcase) }
          items  = items.map { |item| OpenStruct.new(item) }

          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          if items.nil?
            raise(
              ::Scraper::Errors::ScraperItemInvalidFormatError.new(
                params: {
                  items: items,
                  path: 'root.item'
                }
              )
            )
          end
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            if @debug
              Rails.logger.debug "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}"
            end
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================

        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])
          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = "JUVA#{item.store_id}"

          if uid.blank?
            raise(
              ::Scraper::Errors::ScraperItemUidNotFoundError.new(
                params: { item: item }
              )
            )
          end
          # -------------------------------------------------
          # price
          # -------------------------------------------------
          price = [
            begin
              BigDecimal(item.price)
            rescue StandardError
              nil
            end,
            begin
              BigDecimal(item.price)
            rescue StandardError
              nil
            end
          ].clean.min
          return nil if price.nil?

          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                    .where(
                      uid: uid,
                      scraper: PROVIDER_CODE,
                      product_type: PROVIDER_TYPE
                    )
                    .first_or_initialize(
                      uid: uid,
                      scraper: PROVIDER_CODE,
                      product_type: PROVIDER_TYPE
                    )
          # -------------------------------------------------

          # tire fields
          # -------------------------------------------------
          product.model    = item.pr_picture_name.to_s
          product.url      = item.link_store.to_s
          product.width    = BigDecimal(item.pr_width.to_s) if item.pr_width
          product.height   = BigDecimal(item.pr_height.to_s) if item.pr_height
          product.diameter = BigDecimal(item.pr_radius.to_s) if item.pr_radius
          product.speed_rating = item.li_si.tr('0-9', '').downcase if item.li_si
          product.load_index = item.li_si.tr('^0-9', '') if item.li_si
          # -------------------------------------------------

          # season
          # -------------------------------------------------
          product.season = 'ms' if item.season == 'm+s'
          product.season = 'w'  if item.season == 'ziemas riepas'
          product.season = 's'  if item.season == 'vasaras riepas'
          product.season = 's' if product.season.blank?
          # -------------------------------------------------

          # run flat
          # -------------------------------------------------
          # product.run_flat = true if item['run-flat'].downcase === 'jā'
          # -------------------------------------------------

          # studs
          # -------------------------------------------------
          product.studs = false
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------

          if [
            product.model.nil?,
            product.width.nil?,
            product.height.nil?,
            product.diameter.nil?,
            product.speed_rating.nil?
          ].include?(true)
            return nil
          end

          # -------------------------------------------------

          # image
          # -------------------------------------------------
          if Settings['scraper']['add_images'] && product.new_record?
            begin
              if item.image
                image = ::Scraper::Network.new.get_image(item.image)
                url = AwsService::S3.upload(image)
                product.aws_image_url = url
              end
            rescue StandardError => e
              return nil
            end
          end
          # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          return nil if item.pr_brend.to_s.blank?

          product.manufacturer_id = validate_manufacturer(
            item.pr_brend.to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?

          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price: price,
            qty: 0,
            qty_hour: 0,
            qty_days: 0,
            qty_weeks: item.available_pcs
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            product.product_vendors_attributes = if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
                                                   { '0' => product_vendor_params.merge(id: product_vendor.id) }
                                                 else
                                                   { '0' => product_vendor_params }
                                                 end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item.to_h
            product.scraper_inputs_attributes = {
              '0' => { raw_data: item.to_h }
            }
          end
          # --------------------------------------------------
          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

          product
        end
        # ==============================================================================

        private

        def validate_vendor(item)
          vendor = ::Vendor.find_by(name: NAME)

          unless vendor
            raise(
              ::Scraper::Errors::ScraperItemParseFailureError.new(
                params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
              )
            )
          end

          vendor
        end

        def validate_manufacturer(manufacturer_alias, item)
          if manufacturer_alias.to_s.blank?
            raise(
              ::Scraper::Errors::ScraperItemParseFailureError.new(
                params: { message: 'manufacturer not found', raw_data: item }
              )
            )
          end

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(product_type: PROVIDER_TYPE, name: manufacturer_alias)
              .first_or_create!(product_type: PROVIDER_TYPE, name: manufacturer_alias)
          end

          manufacturer
        end
      end
    end
  end
end

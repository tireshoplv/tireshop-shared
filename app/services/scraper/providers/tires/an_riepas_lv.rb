require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class AnRiepasLv
        PROVIDER_CODE = 'an_riepas_lv'.freeze
        PROVIDER_TYPE = 'tire'
        NAME          = 'AnRiepas.lv'.freeze
        URL           = 'https://anriepas.lv/'.freeze


        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================


        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================


        # ==============================================================================
        def run
          puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}'" if @debug

          # get & parse data
          # -----------------------------------------------------
          url    = ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :url)
          source = ::Scraper::Network.new.get_source(url)
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('anriepas', 'item')
          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          raise(
            ::Scraper::Errors::ScraperItemInvalidFormatError.new(
              params: {
                items: items,
                path:  'anriepas.items'
              }
            )
          ) if items.nil?
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}" if @debug
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================


        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])

          # as we only support EUR currency, rn, skip any product with other currencies
          # -------------------------------------------------
          return nil unless item['currency'] == 'EUR'
          # -------------------------------------------------

          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item['id'].to_s

          raise(
            ::Scraper::Errors::ScraperItemUidNotFoundError.new(
              params: { item: item }
            )
          ) if uid.blank?
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = ::BigDecimal.new(item['price']) rescue nil
          return nil if price.nil?
          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                      .where(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
                      .first_or_initialize(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
          # -------------------------------------------------

          # season
          # -------------------------------------------------
          product.season = 'm+s'
          product.season = 'w' if item['season'].include?('winter')
          product.season = 's' if item['season'].include?('summer')
          # -------------------------------------------------

          # vehicle type
          # -------------------------------------------------
          product.vehicle_type = 'car'        if item['type'].nil?
          product.vehicle_type = 'car'        if item['type'] == 'Viegl'
          product.vehicle_type = 'offroad'    if item['type'] == 'Apvidus'
          product.vehicle_type = 'commercial' if item['type'] == 'Komerc'
          # -------------------------------------------------

          # radius & diameter
          # -------------------------------------------------
          if item['radial']
            product.radius   = BigDecimal.new(/[0-9]+/.match(item['radial']).to_a.first)
            product.diameter = BigDecimal.new(product.radius * 2)
          end
          # -------------------------------------------------

          # tire fields
          # -------------------------------------------------
          product.url                     = "https://anriepas.lv/pages/tire.php?id=#{item['id']}&lang=lv"
          product.model                   = item['name'].to_s
          product.studs                   = item['grip'] == 'radz'
          product.width                   = BigDecimal.new(item['width'])
          product.height                  = BigDecimal.new(item['height'])
          product.load_index              = item['loadindex'].to_s            if item['loadindex']
          product.noise_level             = %w[a b c][item['soundlevel'].to_i - 1] if item['soundlevel']
          product.speed_rating            = item['speedindex'].to_s.downcase  if item['speedindex']
          product.wet_breaking_efficiency = item['breaking'].to_s.downcase    if item['breaking']
          product.fuel_efficiency         = item['consumption'].to_s.downcase if item['consumption']
          product.year                    = item['year'].to_i if item['year']
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.width.nil?,
            product.height.nil?,
            product.diameter.nil?,
            product.speed_rating.nil?
          ].include?(true)
            return nil
          end
          # -------------------------------------------------

          # image
          # -------------------------------------------------
          if Settings['scraper']['add_images']
            if product.new_record?
              if item['img']
                image         = ::Scraper::Network.new.get_image(item['img'])
                product.image = ::File.open(image) if image
              end
            end
          end
          # # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          product.manufacturer_id = validate_manufacturer(
            item['brand'].to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?
          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price:     price,
            qty:       item['quant'].to_i,
            qty_hour:  0,
            qty_days:  0,
            qty_weeks: 0,
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              product.product_vendors_attributes = { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              product.product_vendors_attributes = { '0' => product_vendor_params }
            end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          product.scraper_inputs_attributes = {
            '0' => { raw_data: item }
          } if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item
          # --------------------------------------------------

          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

          product
        end
        # ==============================================================================



        private


        def validate_vendor(item)
          vendor = ::Vendor.find_by_alias(PROVIDER_CODE)

          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
            )
          ) unless vendor

          vendor
        end


        def validate_manufacturer(manufacturer_alias, item)
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'manufacturer not found', raw_data: item }
            )
          ) if manufacturer_alias.to_s.blank?

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(            product_type: PROVIDER_TYPE, name: manufacturer_alias )
              .first_or_create!( product_type: PROVIDER_TYPE, name: manufacturer_alias )
          end

          manufacturer
        end

      end
    end
  end
end

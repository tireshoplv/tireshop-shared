require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class RemikLv
        include ::Scraper::Helpers::ScraperHelper
        PROVIDER_CODE = 'remik_lv'.freeze
        PROVIDER_TYPE = 'tire'
        NAME          = 'Remik.lv'.freeze
        URL           = 'https://remik.lv'.freeze


        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================


        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================


        # ==============================================================================
        def run

          # get & parse data
          # -----------------------------------------------------
          vendor_data = ::Scraper::VendorList.remik_lv
          url    = vendor_data.url
          source = ::Scraper::Network.new.get_source(url)
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('tyres', 'tyre')
          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          raise(
            ::Scraper::Errors::ScraperItemInvalidFormatError.new(
              params: {
                items: items,
                path:  'tyres.tyre'
              }
            )
          ) if items.nil?
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          updated_items = []

          items
            .select { |item| item['modifications'].kind_of?(Hash) }
            .select { |item| item['modifications'].keys.length > 0 }
            .each do |item|
            item['modifications']['modification'].each do |mod|
              next if mod.kind_of?(Array)
              next if mod['code'].nil?

              mod_item = item.dup
              mod_item['modifications'] = mod
              updated_items << mod_item
            end
          end

          items = nil # clear memory

          updated_items.each_with_index do |item, index|
            puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{updated_items.length}" if @debug
            create_or_update_product_from_raw_data(item, updated_items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================


        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])

          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item['modifications']['code'].to_s

          raise(
            ::Scraper::Errors::ScraperItemUidNotFoundError.new(
              params: { item: item }
            )
          ) if uid.blank?
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = ::BigDecimal.new(item['modifications']['price1']) rescue nil
          return nil if price.nil?
          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                      .where(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
                      .first_or_initialize(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
          # -------------------------------------------------

          # tire fields
          # -------------------------------------------------
          product.width                   = ::BigDecimal.new(item['modifications']['width'].to_s)    if item['modifications']['width']
          product.height                  = ::BigDecimal.new(item['modifications']['profile'].to_s)  if item['modifications']['profile']
          product.diameter                = ::BigDecimal.new(item['modifications']['diameter'].to_s) if item['modifications']['diameter']
          product.noise_index             = item['modifications']['noise_db'].to_i                   if item['modifications']['noise_db']
          product.load_index              = item['modifications']['li'].to_i                         if item['modifications']['li']
          product.speed_rating            = item['modifications']['si'].to_s.downcase                if item['modifications']['si']
          product.fuel_efficiency         = item['modifications']['fuel'].to_s.downcase              if item['modifications']['fuel']
          product.wet_breaking_efficiency = item['modifications']['grip'].to_s.downcase              if item['modifications']['grip']
          product.model                   = item['model'].to_s
          # product.specification           = item['modifications']['mark'].to_s                       if item['modifications']['mark']
          # -------------------------------------------------

          # season
          # -------------------------------------------------
          product.season = 'w' if item['season'] == 'winter'
          product.season = 's' if item['season'] == 'summer'
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.width.nil?,
            product.height.nil?,
            product.diameter.nil?,
            product.speed_rating.nil?
          ].include?(true)
            return nil
          end
          # -------------------------------------------------

          # image
          # -------------------------------------------------
           
          unless Rails.env.development?
            if Settings['scraper']['add_images']
              begin
                if item["photo"].present?
                  image = ::Scraper::Network.new.get_image(item["photo"]) 
                  url = AwsService::S3.upload(image)
                  product.aws_image_url = url
                end
              rescue StandardError => e
                return nil
              end
            end
          end
          # # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          product.manufacturer_id = validate_manufacturer(
            item['brand'].to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?
          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price:     price,
            qty:       item['modifications']['qty'].to_i,
            qty_hour:  0,
            qty_days:  0,
            qty_weeks: 0,
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              product.product_vendors_attributes = { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              product.product_vendors_attributes = { '0' => product_vendor_params }
            end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          product.scraper_inputs_attributes = {
            '0' => { raw_data: item }
          } if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item
          # --------------------------------------------------

          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

          product
        end
        # ==============================================================================



        private


        def validate_vendor(item)
          vendor = ::Vendor.find_by_alias(PROVIDER_CODE)

          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
            )
          ) unless vendor

          vendor
        end


        def validate_manufacturer(manufacturer_alias, item)
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'manufacturer not found', raw_data: item }
            )
          ) if manufacturer_alias.to_s.blank?

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(            product_type: PROVIDER_TYPE, name: manufacturer_alias )
              .first_or_create!( product_type: PROVIDER_TYPE, name: manufacturer_alias )
          end

          manufacturer
        end

      end
    end
  end
end

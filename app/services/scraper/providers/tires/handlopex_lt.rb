require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class HandlopexLt
        PROVIDER_CODE = 'handlopex_lt'.freeze
        PROVIDER_TYPE = 'tire'
        NAME          = 'Handlopex.lt'.freeze
        URL           = 'https://handlopex.lt/'.freeze


        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================


        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================


        # ==============================================================================
        def run
          puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}'" if @debug

          # get & parse data
          # -----------------------------------------------------
          source = ::Scraper::Network.new.get_ftp_file(
            host:     ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :host),
            port:     ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :port),
            path:     ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :path),
            file:     ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :file),
            username: ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :username),
            password: ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :password)
          )
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('products', 'product')
          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          raise(
            ::Scraper::Errors::ScraperItemInvalidFormatError.new(
              params: {
                items: items,
                path:  'products.product'
              }
            )
          ) if items.nil?
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}" if @debug
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================


        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])


          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item['bar_kodas'].to_s

          return nil if uid.blank?
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = ::BigDecimal.new(item['supplier_price']) rescue nil
          return nil if price.nil?
          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                      .where(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
                      .first_or_initialize(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
          # -------------------------------------------------

          # Vehicle type
          # -------------------------------------------------
          product.vehicle_type = 'agro'       if item['kodas_ls2'] == 'žėmės ūkio technikai'
          product.vehicle_type = 'industrial' if item['kodas_ls2'] == 'industrinei technikai'
          product.vehicle_type = 'car'        if item['kodas_ls2'] == 'lengvosios'
          product.vehicle_type = 'car'        if item['kodas_ls2'] == 'komerciniam transportui'
          product.vehicle_type = 'moto'       if item['kodas_ls2'] == 'motociklams'
          product.vehicle_type = 'loaders'    if item['kodas_ls2'] == 'krautuvai'
          product.vehicle_type = 'suv'        if item['kodas_ls2'] == '4x4 & suv'
          product.vehicle_type = 'truck'      if item['kodas_ls2'] == 'krovininis'
          # -------------------------------------------------

          # tire fields
          # -------------------------------------------------
          product.width    = ::BigDecimal.new(item['width'].to_s)    if item['width']
          product.height   = ::BigDecimal.new(item['height'].to_s)   if item['height']
          product.diameter = ::BigDecimal.new(item['diameter'].to_s) if item['diameter']
          product.ean                     = uid
          product.model                   = item['title'].to_s
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.width.nil?,
            product.height.nil?,
            product.diameter.nil?,
            product.speed_rating.nil?
          ].include?(true)
            return nil
          end
          # -------------------------------------------------

          # image
          # -------------------------------------------------
          if Settings['scraper']['add_images']
            if product.new_record?
              if item['image']
                image         = ::Scraper::Network.new.get_image(item['image'])
                product.image = ::File.open(image) if image
              end
            end
          end
          # # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          product.manufacturer_id = validate_manufacturer(
            item['vendor'].to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?
          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price:     price,
            qty:       0,
            qty_hour:  0,
            qty_days:  item['amount_lt'].to_i,
            qty_weeks: item['amount_pl'].to_i,
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              product.product_vendors_attributes = { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              product.product_vendors_attributes = { '0' => product_vendor_params }
            end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          product.scraper_inputs_attributes = {
            '0' => { raw_data: item }
          } if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item
          # --------------------------------------------------

          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

          product
        end
        # ==============================================================================



        private


        def validate_vendor(item)
          vendor = ::Vendor.find_by_alias(PROVIDER_CODE)

          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
            )
          ) unless vendor

          vendor
        end


        def validate_manufacturer(manufacturer_alias, item)
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'manufacturer not found', raw_data: item }
            )
          ) if manufacturer_alias.to_s.blank?

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(            product_type: PROVIDER_TYPE, name: manufacturer_alias )
              .first_or_create!( product_type: PROVIDER_TYPE, name: manufacturer_alias )
          end

          manufacturer
        end

      end
    end
  end
end

require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class AtlasRiepasLv
        PROVIDER_CODE = 'atlas_riepas_lv'.freeze
        PROVIDER_TYPE = 'tire'
        NAME          = 'Atlas Riepas'.freeze
        URL           = 'http://www.atlasriepas.lv'.freeze


        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================


        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================


        # ==============================================================================
        def run
          puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}'" if @debug

          # get & parse data
          # -----------------------------------------------------
          url    = ::Scraper::Secrets.get(PROVIDER_CODE, PROVIDER_TYPE, :url)
          source = ::Scraper::Network.new.get_source(url)
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('root', 'item')
          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          raise(
            ::Scraper::Errors::ScraperItemInvalidFormatError.new(
              params: {
                items: items,
                path:  'root.items'
              }
            )
          ) if items.nil?
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            puts "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}" if @debug
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================


        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])

          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = item['ean_code'].to_s

          raise(
            ::Scraper::Errors::ScraperItemUidNotFoundError.new(
              params: { item: item }
            )
          ) if uid.blank?
          # -------------------------------------------------

          # price
          # -------------------------------------------------
          price = ::BigDecimal.new(item['retail_price']) rescue nil
          return nil if price.nil?
          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                      .where(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
                      .first_or_initialize(
                        uid:          uid,
                        scraper:      PROVIDER_CODE,
                        product_type: PROVIDER_TYPE
                      )
          # -------------------------------------------------

          # season
          # -------------------------------------------------
          product.season = 'ms'
          product.season = 'w' if item['category_full'] == 'Ziemas riepas'
          product.season = 's' if item['category_full'] == 'Vasaras riepas'
          # -------------------------------------------------

          # vehicle type
          # -------------------------------------------------
          product.vehicle_type = 'car'
          product.vehicle_type = 'suv'   if item.has_key?('tire_weight_type_4x4')
          product.vehicle_type = 'truck' if item.has_key?('tire_weight_type_truck')
          # -------------------------------------------------

          # tire fields
          # -------------------------------------------------
          product.ean             = item['ean_code'].to_s
          product.url             = item['link'].to_s
          product.width           = BigDecimal.new(item['param3'])
          product.model           = item['model']
          product.diameter        = ::BigDecimal.new(/^R([0-9]+)/.match(item['param5']).to_a.last)
          product.fuel_efficiency = item['tire_fuel_economy'].to_s.downcase if item['tire_fuel_economy']
          product.noise_level     = %w[a b c][item['tire_noise_level_index'].to_i - 1] if item['tire_noise_level_index']
          # -------------------------------------------------

          # description
          # -------------------------------------------------
          if item['description']
            product.description = item['description']
            product.description = product.description.gsub('&nbsp;', ' ')
          end
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------
          if [
            product.model.nil?,
            product.width.nil?,
            product.height.nil?,
            product.diameter.nil?,
            product.speed_rating.nil?
          ].include?(true)
            return nil
          end
          # -------------------------------------------------

          # image
          # -------------------------------------------------
          if Settings['scraper']['add_images']
            if product.new_record?
              if item['image']
                image         = ::Scraper::Network.new.get_image(item['image'])
                product.image = ::File.open(image) if image
              end
            end
          end
          # # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          product.manufacturer_id = validate_manufacturer(
            item['manufacturer'].to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?
          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price:     price,
            qty:       item[:in_stock].to_i,
            qty_hour:  0,
            qty_days:  0,
            qty_weeks: 0,
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
              product.product_vendors_attributes = { '0' => product_vendor_params.merge(id: product_vendor.id) }
            else
              product.product_vendors_attributes = { '0' => product_vendor_params }
            end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          product.scraper_inputs_attributes = {
            '0' => { raw_data: item }
          } if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item
          # --------------------------------------------------

          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

          product
        end
        # ==============================================================================



        private


        def validate_vendor(item)
          vendor = ::Vendor.find_by_alias(PROVIDER_CODE)

          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
            )
          ) unless vendor

          vendor
        end


        def validate_manufacturer(manufacturer_alias, item)
          raise(
            ::Scraper::Errors::ScraperItemParseFailureError.new(
              params: { message: 'manufacturer not found', raw_data: item }
            )
          ) if manufacturer_alias.to_s.blank?

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(            product_type: PROVIDER_TYPE, name: manufacturer_alias )
              .first_or_create!( product_type: PROVIDER_TYPE, name: manufacturer_alias )
          end

          manufacturer
        end

      end
    end
  end
end

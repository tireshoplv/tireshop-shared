# frozen_string_literal: true

require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class EgzotikaLt
        PROVIDER_CODE = 'egzotika_lt'
        PROVIDER_TYPE = 'tire'
        NAME          = 'Egzotika.lt'
        URL           = 'https://www.egzotika.lt/'

        # ==============================================================================
        def initialize(debug: false, count: nil)
          @debug = debug
        end
        # ==============================================================================

        # ==============================================================================
        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end
        # ==============================================================================

        # ==============================================================================
        def run
          # get & parse data
          # -----------------------------------------------------
          url    = 'https://www.b2b.egzotika.lt/?cl=xmlgenerator&lang=0&partnerid=Tyre_list_xml&uid=9b1a5dda8c96cde8587850bb29a24d10'
          source = ::Scraper::Network.new.get_source(url)
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig('offers', 'offer').map { |item| item.transform_keys(&:downcase) }
          items  = items.map { |item| OpenStruct.new(item) }

          # -----------------------------------------------------

          # raise error if failed to parse data
          # -----------------------------------------------------
          if items.nil?
            raise(
              ::Scraper::Errors::ScraperItemInvalidFormatError.new(
                params: {
                  items: items,
                  path: 'offers.offer'
                }
              )
            )
          end
          # -----------------------------------------------------

          # loop through items and save them (or log if error)
          # -----------------------------------------------------
          items.each_with_index do |item, index|
            if @debug
              Rails.logger.debug "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}"
            end
            create_or_update_product_from_raw_data(item, items)
          end
          # -----------------------------------------------------
        end
        # ==============================================================================

        # ==============================================================================
        def create_or_update_product_from_raw_data(item, _items = [])
          # this is uniq identifier each scraper will make,
          # to make record completely uniq in the database
          # this must exist.
          # -------------------------------------------------
          uid = "EGZOTIKA#{item.id}"

          if uid.blank?
            raise(
              ::Scraper::Errors::ScraperItemUidNotFoundError.new(
                params: { item: item }
              )
            )
          end
          # -------------------------------------------------
          # price
          # -------------------------------------------------
          price = [
            begin
              BigDecimal(item.price)
            rescue StandardError
              nil
            end,
            begin
              BigDecimal(item.price)
            rescue StandardError
              nil
            end
          ].clean.min
          return nil if price.nil?

          # -------------------------------------------------

          # find or initialize product
          # -------------------------------------------------
          product = ::Product
                    .where(
                      uid: uid,
                      scraper: PROVIDER_CODE,
                      product_type: PROVIDER_TYPE
                    )
                    .first_or_initialize(
                      uid: uid,
                      scraper: PROVIDER_CODE,
                      product_type: PROVIDER_TYPE
                    )
          # -------------------------------------------------

          # tire fields
          # -------------------------------------------------
          product.model    = item.model.to_s
          product.url      = item.url.to_s
          product.width    = BigDecimal(parse_fullparams(item.fullparams).width.to_s) if item.fullparams
          product.height   = BigDecimal(parse_fullparams(item.fullparams).height.to_s) if item.fullparams
          product.diameter = BigDecimal(parse_fullparams(item.fullparams).radius.to_s) if item.fullparams
          product.speed_rating = item.si.to_s.downcase if item.si
          product.load_index = item.li.to_s if item.li
          # -------------------------------------------------

          # season
          # -------------------------------------------------
          product.season = 'ms' if item.season == 'Allseason'
          product.season = 'w'  if item.season == 'Winter'
          product.season = 's'  if item.season == 'Summer'
          product.season = 's' if product.season.blank?
          # -------------------------------------------------

          # run flat
          # -------------------------------------------------
          # product.run_flat = true if item['run-flat'].downcase === 'jā'
          # -------------------------------------------------

          # studs
          # -------------------------------------------------
          product.studs = false
          # -------------------------------------------------

          # check most important fields and SKIP if they are missing
          # -------------------------------------------------

          if [
            product.model.nil?,
            product.width.nil?,
            product.height.nil?,
            product.diameter.nil?,
            product.speed_rating.nil?
          ].include?(true)
            return nil
          end

          # -------------------------------------------------

          # image
          # -------------------------------------------------
          if Settings['scraper']['add_images'] && product.new_record?
            begin
              if item.img
                image = ::Scraper::Network.new.get_image(item.img)
                url = AwsService::S3.upload(image)
                binding.pry
                product.aws_image_url = url
              end
            rescue StandardError => e
              return nil
            end
          end
          # -------------------------------------------------

          # manufacturer
          # -------------------------------------------------
          return nil if item.mtitle.to_s.blank?

          product.manufacturer_id = validate_manufacturer(
            item.mtitle.to_s.downcase.to_s.strip,
            item
          ).try(:id)
          return nil if product.manufacturer_id.nil?

          # -------------------------------------------------

          # Vendor fields
          # --------------------------------------------------
          vendor = validate_vendor(item)

          product_vendor_params = {
            vendor_id: vendor.id,
            price: price,
            qty: 0,
            qty_hour: 0,
            qty_days: 0,
            qty_weeks: item.available_pcs
          }

          if product.id.nil?
            product.product_vendors_attributes = { '0' => product_vendor_params }
          else
            product.product_vendors_attributes = if (product_vendor = product.product_vendors.where(vendor_id: vendor.id).first)
                                                   { '0' => product_vendor_params.merge(id: product_vendor.id) }
                                                 else
                                                   { '0' => product_vendor_params }
                                                 end
          end
          # --------------------------------------------------

          # raw data
          # --------------------------------------------------
          if product.id.nil? || product.scraper_inputs.last.try(:raw_data) != item.to_h
            product.scraper_inputs_attributes = {
              '0' => { raw_data: item.to_h }
            }
          end
          # --------------------------------------------------
          # save or log
          # --------------------------------------------------
          if product.save
            ::ScraperInvalidRecord.clear(product)
          else
            ::ScraperInvalidRecord.log(product)
          end
          # --------------------------------------------------

          product
        end
        # ==============================================================================

        private

        def validate_vendor(item)
          vendor = ::Vendor.find_by(name: NAME)

          unless vendor
            raise(
              ::Scraper::Errors::ScraperItemParseFailureError.new(
                params: { message: 'vendor not found', vendor: PROVIDER_CODE, raw_data: item }
              )
            )
          end

          vendor
        end

        def validate_manufacturer(manufacturer_alias, item)
          if manufacturer_alias.to_s.blank?
            raise(
              ::Scraper::Errors::ScraperItemParseFailureError.new(
                params: { message: 'manufacturer not found', raw_data: item }
              )
            )
          end

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(product_type: PROVIDER_TYPE, name: manufacturer_alias)
              .first_or_create!(product_type: PROVIDER_TYPE, name: manufacturer_alias)
          end

          manufacturer
        end

        def parse_fullparams(fullparams)
          delimiters = ['/', 'R']
          results=fullparams.split(Regexp.union(delimiters))
          return OpenStruct.new({'width'=>results[0],'height'=>results[1],'radius'=>results[2]})
        end
      end
    end
  end
end

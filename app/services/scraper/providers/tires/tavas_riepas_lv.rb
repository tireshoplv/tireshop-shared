# frozen_string_literal: true

require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class TavasRiepasLv
        include ::Scraper::Helpers::ScraperHelper
        PROVIDER_CODE = 'tavas_riepas_lv'
        PROVIDER_TYPE = 'tire'
        NAME          = 'TavasRiepas.lv'
        URL           = 'https://tavas_riepas.lv'
        DIG_FIELD_1   = 'products'
        DIG_FIELD_2   = 'tyre'

        def initialize(debug: false, count: nil)
          @debug = debug
        end

        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end

        def run
          # get & parse data
          # -----------------------------------------------------
          vendor_data = ::Scraper::VendorList.tavas_riepas_lv
          url    = vendor_data.url
          source = ::Scraper::Network.new.get_source(url)
          json   = ::Scraper::Converter.xml_to_json(source)
          items  = json.dig(DIG_FIELD_1, DIG_FIELD_2)
          @field_definitions = ::Scraper::Helpers::FieldDefinitionsHelper.__send__(PROVIDER_CODE)
          @vendor ||= ::Vendor.find_by(name: NAME)
          items = items.map do |item|
            define_items(item)
          end

          # the idea is to make sure that all theoretically valid objects are prepared via map.
          # the logic of #prepare_product_from_raw_data should not be touched
          # all the fields are over-parsed/prepare before passing to importer
          # necessary mutations can be done via map as well. if there are manufacturers to be parsed out of other fields - we do it here
          # or we can always add private method within this namespace for this vendors purposes exclusively.

          items = items.map do |item|
            item.uid = "tavasriepas-#{item.uid}"
            item.speed_rating = item.speed_rating.gsub(/\d+/,"") if item.speed_rating
            item.load_index = item.load_index.gsub(/\D/, '') if item.load_index
            item.image_url = item.image_url["photo"] if item.image_url
            if item.vehicle_type
              item.vehicle_type = "pcr" if item.vehicle_type == "false"
              item.vehicle_type = "moto" if item.vehicle_type == "true"
            end
            item
          end

          if items.nil?
            raise(
              ::Scraper::Errors::ScraperItemInvalidFormatError.new(
                params: {
                  items: items,
                  path: 'root.item'
                }
              )
            )
          end

          @items_to_update = []
          @items_to_create = []

          items.each_with_index do |item, index|
            if @debug
              Rails.logger.debug "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}"
            end

            prepare_product_from_raw_data(item, items, PROVIDER_CODE, PROVIDER_TYPE)
          end

          bulk_create_items(@items_to_create)
          bulk_update_items(@items_to_update)
        end

        private

        def validate_manufacturer(manufacturer_alias, item)
          if manufacturer_alias.to_s.blank?
            raise(
              ::Scraper::Errors::ScraperItemParseFailureError.new(
                params: { message: 'manufacturer not found', raw_data: item }
              )
            )
          end

          return @manufacturer if @manufacturer_alias == manufacturer_alias

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first
          @manufacturer = manufacturer
          @manufacturer_alias = manufacturer_alias

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(product_type: PROVIDER_TYPE, name: manufacturer_alias)
              .first_or_create!(product_type: PROVIDER_TYPE, name: manufacturer_alias)
          end

          manufacturer
        end
      end
    end
  end
end

# frozen_string_literal: true

require 'bigdecimal'

module Scraper
  module Providers
    module Tires
      class TireshopLv
        include ::Scraper::Helpers::ScraperHelper
        PROVIDER_CODE = 'tireshop_lv'
        PROVIDER_TYPE = 'tire'
        NAME          = 'Tireshop.Lv'
        URL           = 'tireshop.lv'
        DIG_FIELD_1   = "items"

        def initialize(debug: false, count: nil)
          @debug = debug
        end

        def self.info
          { name: NAME, code: PROVIDER_CODE, type: PROVIDER_TYPE, url: URL }
        end

        def run

          #mandatory fields 
          
          vendor_data = ::Scraper::VendorList.tireshop_lv
          url    = vendor_data.url
          source = ::Scraper::Network.new.get_source(url)
          json   = JSON.parse(source)
          items  = json.dig(DIG_FIELD_1)
          @vendor ||= ::Vendor.find_by(name: NAME)
          @field_definitions = ::Scraper::Helpers::FieldDefinitionsHelper.__send__(Scraper::Providers::Tires::TireshopLv::PROVIDER_CODE)

          # remapping the fields to struct objects with definition stated in scraper_helper.
          # this effectivelly allows us to reuse majority of individual scrapers for every vendor without touching the parsing logic.
          # the only thing necessary to add a new vendor scraper is to add field in field definitions helper, under method that corresponds to name of vendor.

          items = items.map do |item|
            define_items(item)
          end

          items.map do |item| 
            item.uid = "tireshop-#{item.uid}"
            item.speed_rating = item.speed_rating.gsub(/\d+/,"") if item.speed_rating
            item.load_index = item.load_index.gsub(/\D/, '') if item.load_index
            item.vehicle_type = "pcr"
          end

          # raise error if failed to parse data
          if items.nil?
            raise(
              ::Scraper::Errors::ScraperItemInvalidFormatError.new(
                params: {
                  items: items,
                  path: 'root.item'
                }
              )
            )
          end

          @items_to_update = []
          @items_to_create = []

          # loop through items and save them (or log if error)

          items.each_with_index do |item, index|
            if @debug
              Rails.logger.debug "Scraper: importing '#{PROVIDER_TYPE}' - '#{PROVIDER_CODE}' | #{index + 1}/#{items.length}"
            end

            prepare_product_from_raw_data(item, items, PROVIDER_CODE, PROVIDER_TYPE)
          end

          bulk_create_items(@items_to_create)
          bulk_update_items(@items_to_update)
        end

        private

        # manufacturer validator is the only validator that needs to be vendor specific so far.
        # data inconsistency may conflict with alias architecture.

        def validate_manufacturer(manufacturer_alias, item)
          if manufacturer_alias.to_s.blank?
            raise(
              ::Scraper::Errors::ScraperItemParseFailureError.new(
                params: { message: 'manufacturer not found', raw_data: item }
              )
            )
          end

          return @manufacturer if @manufacturer_alias == manufacturer_alias

          manufacturer = ::Manufacturer.by_aliases(manufacturer_alias).first
          @manufacturer = manufacturer
          @manufacturer_alias = manufacturer_alias

          if manufacturer.nil?
            ManufacturerUnassignedAlias
              .where(product_type: PROVIDER_TYPE, name: manufacturer_alias)
              .first_or_create!(product_type: PROVIDER_TYPE, name: manufacturer_alias)
          end

          manufacturer
        end
      end
    end
  end
end

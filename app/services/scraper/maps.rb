module Scraper
  class Maps 
    USAGE_CODES_MAP = {
      'pcr' => Product::CAR,
      'vettura' => Product::CAR,
      'viegl' => Product::CAR,
      'passenger car' => Product::CAR,
      'psr' => Product::CAR,
      'lengvasis transportas' => Product::CAR,
      'maastur' => Product::CAR,
      'auto' => Product::CAR,
      'sõiduauto/maastur' => Product::CAR,
      'car' => Product::CAR,
      'other' => Product::CAR,
      'lengvosios' => Product::CAR,
      'pkw' => Product::CAR,
      'imp' => Product::CAR,
      'komerc' => Product::BUS,
      'kaubik' => Product::BUS,
      'komerciniam transportui' => Product::BUS,
      'komercinis'              => Product::BUS,
      'wan'                     => Product::BUS,
      'mot'                     => Product::MOTO,
      'atv'                     => Product::MOTO,
      'motociklams'             => Product::MOTO,
      'moto'                    => Product::MOTO,
      'mootorratas'             => Product::MOTO,
      'agro'                    => Product::AGRO,
      'žėmės ūkio technikai'    => Product::AGRO,
      'industrinei technikai'   => Product::AGRO,
      "anhänger"                => Product::AGRO,
      "fuoristrada"             => Product::SUV,
      "jeep / 4x4"              => Product::SUV,
      '4x4 & suv'               => Product::SUV,
      'apvidus'                 => Product::SUV,
      '4x4'                     => Product::SUV,
      'off'                     => Product::SUV,
      "light truck"             => Product::TRUCK,
      'litruck'                 => Product::TRUCK,
      'veoauto'                 => Product::TRUCK,
      'truck'                   => Product::TRUCK,
      'krovininis'              => Product::TRUCK,
      'indu'                    => Product::AGRO,
      'kita'                    => nil, # TODO: ???
      'krautuvai'               => nil, # TODO: ???
      'atv rehvid'              => nil, # TODO: add to 'usages' table?
    }
  end
end
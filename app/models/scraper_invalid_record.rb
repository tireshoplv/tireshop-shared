class ScraperInvalidRecord < ApplicationRecord


  # serialize
  # ---------------------------------------------------------------
  serialize :product,  Hash
  serialize :raw_data, Hash
  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------
  validates :uid,
            :field,
            :scraper,
            :product,
            :raw_data,
            presence: true
  # ---------------------------------------------------------------


  # class methods
  # ---------------------------------------------------------------

  def self.clear(product)
    ::ScraperInvalidRecord.where(uid: product.uid, scraper: product.scraper).destroy_all
  end


  # TODO: refactor this method
  def self.log(product)
    all_errors = JSON.parse(product.errors.to_json)

    all_errors.keys.each do |error_field|
      all_errors[error_field].each do |field_error|
        skip_invalid_record_create = false

        # skip association, as we already log it's errors
        # -------------------------------------
        next if Product
                  .reflect_on_all_associations(:has_one)
                  .map(&:name)
                  .map(&:to_s)
                  .include?(error_field)
        # -------------------------------------

        # log general error
        # -------------------------------------
        unless skip_invalid_record_create
          ::ScraperInvalidRecord
            .where(
              uid:     product.uid,
              field:   error_field,
              error:   field_error,
              scraper: product.scraper
            )
            .first_or_create!(
              uid:      product.uid,
              field:    error_field,
              error:    field_error,
              scraper:  product.scraper,
              product:  product.to_informative_json,
              raw_data: product.scraper_inputs.last.raw_data
            )
        end
        # -------------------------------------

      end
    end
  end

  # ---------------------------------------------------------------


end

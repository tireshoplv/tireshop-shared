class Order < ApplicationRecord
  include Versionable
  include GenerateHashId

  ORDER_STATUSES = {
    "new_order" => "new",
  }

  ORDER_TYPES = {
    "admin" => "admin_order",
    "buyer_user" => "user_purchase_product",
  }
  # associations
  # ---------------------------------------------------------------
  belongs_to :user

  has_many :transactions, class_name: 'OrderTransaction', dependent: :destroy
  has_many :payments

  has_many :order_product_vendors, dependent: :destroy
  has_many :order_other_items,     dependent: :destroy

  accepts_nested_attributes_for :transactions,          allow_destroy: false
  accepts_nested_attributes_for :order_product_vendors, allow_destroy: true, reject_if: proc { |attrs| attrs['product_vendor_id'].to_s.blank? }
  accepts_nested_attributes_for :order_other_items,     allow_destroy: true
  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------

  validates :user_id,
            presence: true

  validates :price,
            numericality: { greater_than: 0 },
            presence:     true,
            if:           Proc.new { |m| m.paid? }

  validates :paid_at,
            presence: true,
            if:       Proc.new { |m| m.paid? }

  # ---------------------------------------------------------------


  # localization
  # ---------------------------------------------------------------

  enumerize :order_type,
            in:         I18n.t('enumerize.order.order_type').keys,
            i18n_scope: 'enumerize.order.order_type',
            predicates: { prefix: true }

  enumerize :status,
            in:         I18n.t('enumerize.order.status').keys,
            i18n_scope: 'enumerize.order.status',
            predicates: { prefix: true }

  enumerize :delivery_method,
            in:         I18n.t('enumerize.order.delivery_method').keys,
            i18n_scope: 'enumerize.order.delivery_method',
            predicates: { prefix: true }

  enumerize :payment_method,
            in:         I18n.t('enumerize.order.payment_method').keys,
            i18n_scope: 'enumerize.order.payment_method',
            predicates: { prefix: true }

  # ---------------------------------------------------------------


  # aut-methods
  # ---------------------------------------------------------------
  before_validation :generate_order_total_price, :generate_order_paid_at
  # ---------------------------------------------------------------


  # Disable deletion
  # ---------------------------------------------------------------
  # TODO: test for destroy and delete_all
  # TODO: move to concern and add to other payment models
  # before_destroy :stop_destroy, prepend: true
  #
  # def stop_destroy
  #   raise 'Order deletion is not allowed'
  # end
  # ---------------------------------------------------------------


  # instance methods
  # ---------------------------------------------------------------


  def refund_to_wallet(user_id)
    user = User.find(user_id) # ensure user exist

    return false unless self.paid?

    # TODO: find out why can? doesnt work here.
    #       This should be fixed ASAP as it's quite dangerous to not have this check also here.
    # return false unless user.can?(:refund, self) # double/triple check if user can refund
    # Order.accessible_by(Ability.new(current_user).ability, :refund).find_by(id: @order.id)

    self.update(
      editor_id:               user.id,
      version_event:           'admin_update',
      paid:                    false,
      transactions_attributes: [
                                 { user_id: self.user_id, transaction_type: 'refund', amount: self.price  },
                                 { user_id: 0,            transaction_type: 'refund', amount: -self.price },
                               ]
    )
  end


  def refund(user_id)
    user = User.find(user_id) # ensure user exist

    return false unless self.paid?

    # TODO: find out why can? doesnt work here.
    #       This should be fixed ASAP as it's quite dangerous to not have this check also here.
    # return false unless user.can?(:refund, self) # double/triple check if user can refund
    # Order.accessible_by(Ability.new(current_user).ability, :refund).find_by(id: @order.id)

    self.update(
      editor_id:               user.id,
      version_event:           'admin_update',
      paid:                    false,
      transactions_attributes: [
                                 { user_id: 0, transaction_type: 'refund', amount: -self.price },
                               ]
    )
  end


  def mark_as_paid(user_id)
    user = User.find(user_id) # ensure user exist

    return false if self.paid?

    # TODO: find out why can? doesnt work here.
    #       This should be fixed ASAP as it's quite dangerous to not have this check also here.
    # return false unless user.can?(:mark_as_paid, self) # double/triple check if user can mark_as_paid
    # Order.accessible_by(Ability.new(current_user).ability, :mark_as_paid).find_by(id: @order.id)

    self.update(
      editor_id:               user.id,
      version_event:           'admin_update',
      paid:                    true,
      transactions_attributes: [
                                 { user_id: 0,            transaction_type: 'income',   amount: self.price  },
                                 { user_id: self.user_id, transaction_type: 'purchase', amount: -self.price },
                                 { user_id: self.user_id, transaction_type: 'deposit',  amount: self.price  }
                               ]
    )
  end


  def product_vendors_include?(product_vendor)
    self.order_product_vendors.select { |pv| pv.product_vendor_id == product_vendor.id }.any?
  end


  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------


  def generate_order_total_price
    total_price = []
    total_price += self.order_product_vendors.map { |item| item.price.to_i * item.count.to_i }
    total_price += self.order_other_items.map     { |item| item.price.to_i * item.count.to_i }
    total_price << self.assembly_price if self.assembly?
    self.price = total_price.sum
  end


  def generate_order_paid_at
    if self.paid.nil?
      self.paid_at = nil
    else
      self.paid_at = Time.current if self.paid_at.nil?
    end
  end


  def set_version
    self.version_body = {

      user_id: self.user_id,
      order_type: self.order_type,
      price: self.price,

      delivery_method: self.delivery_method,
      payment_method:  self.payment_method,
      paid:            self.paid,
      paid_at:         self.paid_at,

      user_notes:  self.user_notes,
      admin_notes: self.admin_notes,

      assembly:       self.assembly,
      assembly_price: self.assembly_price,

      order_product_vendors: self.order_product_vendors.map { |order_product_vendor|
        {
          product_vendor_id: order_product_vendor.product_vendor_id,
          count:             order_product_vendor.count,
          price:             order_product_vendor.price
        }
      },

      order_other_items: self.order_other_items.map { |order_other_item|
        {
          name:      order_other_item.name,
          count:     order_other_item.count,
          price:     order_other_item.price,
          vendor_id: order_other_item.vendor_id
        }
      }

    }
  end

  # ---------------------------------------------------------------

end

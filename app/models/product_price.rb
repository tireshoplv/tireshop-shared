# This table is used in front-end to quickly find price, including discounts and markups
class ProductPrice < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :product
  belongs_to :vendor
  belongs_to :product_vendor
  belongs_to :manufacturer
  belongs_to :markup
  belongs_to :discount, required: false
  # ---------------------------------------------------------------

  # validations
  # ---------------------------------------------------------------
  validates :price,
            :original_price,
            numericality: { greater_than: 0 },
            presence:     true

  validates :markup_value,
            :discount_value,
            numericality: { greater_than_or_equal_to: 0 }
  # ---------------------------------------------------------------

end

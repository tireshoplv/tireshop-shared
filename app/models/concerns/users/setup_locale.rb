require 'active_support/concern'

module Users
  module SetupLocale
    extend ::ActiveSupport::Concern

    included do

      aasm column: :locales do
        ::I18n.t('enumerize.user.locale').keys.map do |key|
          state key
        end
      end

      enumerize :locale,
                in:         ::I18n.t('locales').keys,
                i18n_scope: 'locales',
                predicates: { prefix: true }

    end
  end
end

require 'active_support/concern'

module SetupVisible
  extend ::ActiveSupport::Concern

  included do

    scope :visible,     -> { where(visible: true) }
    scope :not_visible, -> { where(visible: false) }

  end
end

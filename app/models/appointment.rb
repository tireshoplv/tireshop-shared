class Appointment < ActiveRecord::Base
  attr_accessor :full_time

  AVAILABLE_SERVICE_BOXES = [
    1,
    2
  ]

  before_validation :set_service_box

  validates :auto_number,
    :auto_brand,
    :auto_model,
    :full_name,
    :phone_number,
    :start_time,
    :end_time,
    :email,
    presence: true

  validates :auto_number, length: { maximum: 10 }
  validates :auto_brand, :auto_model, :full_name, :email, length: { maximum: 100 }
  validates :notes, length: { maximum: 255 }
  validates :phone_number, length: { maximum: 20 }

  validates_uniqueness_of :start_time, scope: :service_box
  validates_uniqueness_of :end_time, scope: :service_box
  validates_uniqueness_of :auto_number, scope: [:start_time, :end_time]

  validate :appointment_is_in_working_hours
  validate :end_time_less_than_start_time
  validate :start_time_less_than_now
  validate :holiday_date

  # Disabled to allow non-latvian and individual number plates
  # In future - make a regex list for different national number plates
  # validates :auto_number, format: { with: /\A[A-Z]{2}\d{1,4}\z/ }
  validates :phone_number, format: { with: /\A(\+*[\+0-9;,\s]{8,25})\z/ }, if: ->{ phone_number.present? }
  validates :email, format: { with:  /\A[\-A-Za-z0-9_+\.~\-\?]+\@([\-A-Z\-a-z0-9]+\.)+[A-Z\-a-z0-9]{2,4}\Z/ }, if: ->{ email.present? }

  scope :for_day, ->(start_time, end_time) { where('start_time >= ? AND end_time <= ?', start_time, end_time) }
  scope :for_time, ->(start_time, end_time) { where('(start_time = :start_time OR end_time = :end_time) OR (start_time <= :start_time AND end_time >= :end_time)', start_time: start_time, end_time: end_time) }

  before_create :set_destroy_uuid

  class << self 
    def working_hours
      WorkingHour.working_hours
    end
  
    def by_service_box(service_box)
      return unless AVAILABLE_SERVICE_BOXES.include? service_box
      self.where(service_box: service_box)
    end
  
    def service_box_busy?(service_box)
      all.where(service_box: service_box).present?
    end
  
    def with_both_service_boxes_busy 
      service_box_1_appointments = all.where(service_box: 1).map { |a| [a.start_time, a.end_time] }
      service_box_2_appointments = all.where(service_box: 2).map { |a| [a.start_time, a.end_time] }
  
      service_box_1_appointments & service_box_2_appointments
    end
  end

  def to_text
    text_arr = []

    text_arr << "#{I18n.t('activerecord.attributes.appointment.full_time')}: #{start_time.strftime('%Y.%m.%d %H:%M')} - #{end_time.strftime('%H:%M')}"

    [:auto_number, :auto_brand, :auto_model,
      :full_name, :phone_number, :disk_diameter,
      :email, :tires_stored, :tires_stored_notes,
      :tires_bought, :tires_bought_notes,:notes,
      :service_box
    ].each do |col|
      tanslation = I18n.t("activerecord.attributes.appointment.#{col}")
      text_arr << "#{tanslation}: #{self.send(col)}"
    end
    text_arr.join("\n\n")
  end

  def title
    "#{auto_brand}/#{auto_model}. Pieraksts uz riepu maiņu"
  end

  def google_id
    if Rails.env.development?
      "v2tappointment#{self.id}"
    else
      "v2appointment#{self.id}"
    end
  end

  private

  def set_service_box 
    if self.class.for_time(start_time, end_time).where(service_box: service_box).present?
      self.service_box = 2 
    end
  end

  def set_destroy_uuid
    self.destroy_uuid = SecureRandom.hex(10)
  end

  def appointment_is_in_working_hours
    day_start_time = WorkingHour.working_time_limit_at_(:start, start_time)
    day_end_time = WorkingHour.working_time_limit_at_(:end, end_time)
  
    unless day_start_time.present? 
      errors.add :start_time, "Appointment's date has no service working hours" 
      return
    end

    unless day_end_time.present? 
      errors.add :end_time, "Appointment's date has no service working hours"
      return
    end
    
    errors.add :start_time, "Appointment start time out of day's working hours" if start_time < day_start_time
    errors.add :end_time, "Appointment end time out of day's working hours" if end_time > day_end_time
  end

  def end_time_less_than_start_time
    return unless errors.empty?
    errors.add :end_time, :invalid unless end_time > start_time
  end

  def start_time_less_than_now
    return unless errors.empty?
    errors.add :start_time, :invalid unless start_time > Time.zone.now
  end

  def holiday_date
    return unless errors.empty?
    errors.add :start_time, :invalid if start_time.wday == 0
  end
end

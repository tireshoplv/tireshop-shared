class UserCartProductVendor < ApplicationRecord


  # associations
  # ---------------------------------------------------------------
  belongs_to :user
  belongs_to :product
  belongs_to :product_vendor
  # ---------------------------------------------------------------


  # auto-methods
  # ---------------------------------------------------------------
  after_save    :update_products_in_cart_count
  after_destroy :update_products_in_cart_count
  after_commit  :add_user_cart_historical_product, on: [:create]
  # ---------------------------------------------------------------


  # instance methods
  # ---------------------------------------------------------------

  def self.generate_total_price
    markup = self.first.user.markup

    self.order(:id).map do |cart_product_vendor|
      cart_product_vendor.product_vendor.generate_price(markup: markup, count: cart_product_vendor.count).price
    end.sum
  end

  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------


  def update_products_in_cart_count
    self.user.update(products_in_cart: self.user.cart_product_vendors.count)
  end


  def add_user_cart_historical_product
    params = { user_id: self.user_id, product_vendor_id: self.product_vendor.id, product_id: self.product_id }

    UserCartHistoricalProduct
      .where(params)
      .first_or_create!(params)
  end


  # ---------------------------------------------------------------


end

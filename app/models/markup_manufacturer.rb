class MarkupManufacturer < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :markup, touch: true
  belongs_to :manufacturer, required: false
  # ---------------------------------------------------------------

end

class ProductVendor < ApplicationRecord


  # associations
  # ---------------------------------------------------------------
  belongs_to :product, touch: true
  belongs_to :vendor
  has_many   :prices,                   class_name: 'ProductVendorPrice',        dependent: :destroy
  has_many   :cart_product_vendors,     class_name: 'UserCartProductVendor',     dependent: :restrict_with_error
  has_many   :cart_historical_products, class_name: 'UserCartHistoricalProduct', dependent: :restrict_with_error

  accepts_nested_attributes_for :prices, allow_destroy: false
  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------
  validates :price,
            numericality: { greater_than: 0 },
            presence:     true,
            if:           lambda { |m| m.product.scraper.nil? }
  # ---------------------------------------------------------------


  # auto-methods
  # ---------------------------------------------------------------
  after_save :create_price_history
  # ---------------------------------------------------------------


  # instance methods
  # ---------------------------------------------------------------

  def admin_option_title
    [
      self.product.admin_option_title,
      self
        .generate_price(markup: Markup.first)
        .original_price(as_string: true, show_symbol: true, show_vendor: true)
    ].join(' ')
  end

  def generate_price(markup: nil, count: 1)
    ProductPriceCalculator.new(
      count:          count,
      markup:         markup,
      symbol:         Product::CURRENCY_SYMBOL,
      product_vendor: self
    )
  end

  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------

  def create_price_history
    if self.prices.last.nil? || self.prices.last.price != self.price
      self.prices.create(
        price:           self.price,
        vendor_id:       self.vendor.id,
        product_id:      self.product.id,
        manufacturer_id: self.product.manufacturer.id
      )
    end
  end

  # ---------------------------------------------------------------

end

class DiscountFilteredProductField < ApplicationRecord


  # associations
  # ---------------------------------------------------------------
  belongs_to :discount_filtered_product, touch: true
  # ---------------------------------------------------------------


  # localization
  # ---------------------------------------------------------------

  enumerize :field,
            in:         I18n.t('enumerize.discount_filtered_product_field.field').keys,
            i18n_scope: 'enumerize.discount_filtered_product_field.field',
            predicates: { prefix: true }

  enumerize :product_type,
            in:         I18n.t('enumerize.product.product_type').keys,
            i18n_scope: 'enumerize.product.product_type',
            predicates: { prefix: true }

  # ---------------------------------------------------------------


end

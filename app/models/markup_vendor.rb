class MarkupVendor < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :markup, touch: true
  belongs_to :vendor, required: false
  # ---------------------------------------------------------------

end

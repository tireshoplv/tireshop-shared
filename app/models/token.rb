class Token < ApplicationRecord
    #To-DO: noskaidrot kā varu šeit tikt pie user_id

    def self.set_all_other_user_tokens_to_expired(valid_token)
        Token.where.not(value: valid_token).where(user_id:3).update_all(aasm_state: 'expired')
    end

    def self.create_with_generated_token
        token = Token.create(value:SecureRandom.uuid, user_id:3, aasm_state:'active').value
        set_all_other_user_tokens_to_expired(token)
    end
end

class MarkupFilteredProduct < ApplicationRecord


  # associations
  # ---------------------------------------------------------------
  belongs_to :markup, touch: true
  has_many   :fields, class_name: 'MarkupFilteredProductField', dependent: :destroy

  accepts_nested_attributes_for :fields, allow_destroy: true, reject_if: proc { |attrs| attrs['field'].to_s.blank? }
  # ---------------------------------------------------------------


end

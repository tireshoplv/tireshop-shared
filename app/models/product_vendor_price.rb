class ProductVendorPrice < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :vendor
  belongs_to :product
  belongs_to :manufacturer
  belongs_to :product_vendor
  # ---------------------------------------------------------------

  # validations
  # ---------------------------------------------------------------
  validates :price,
            numericality: { greater_than: 0 },
            presence:     true
  # ---------------------------------------------------------------

end

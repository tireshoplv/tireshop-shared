class LineItemProduct < ApplicationRecord
  belongs_to :line_item 
  belongs_to :product
end

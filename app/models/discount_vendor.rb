class DiscountVendor < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :discount, touch: true
  belongs_to :vendor, required: false
  # ---------------------------------------------------------------

end

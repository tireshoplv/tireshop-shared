class Session < ApplicationRecord
  belongs_to :user 
  after_create :inactivate_old_tokens
  include AASM 
  
  aasm do 
    state :active, initial: true
    state :inactive

    event :inactivate do 
      transitions from: :active, to: :inactive 
    end

    event :activate do 
      transitions from: :inactive, to: :active 
    end
  end

  def inactivate_old_tokens 
    Session.where(aasm_state: "active")
    .where.not(id: self.id)
    .map{ |x| x.inactivate }
  end

end

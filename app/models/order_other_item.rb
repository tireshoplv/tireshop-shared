class OrderOtherItem < ApplicationRecord


  # associations
  # ---------------------------------------------------------------
  belongs_to :order,  touch: true
  belongs_to :vendor
  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------

  validates :name, :price,
            presence: true

  validates :price,
            numericality: { greater_than: 0 },
            presence:     true

  # ---------------------------------------------------------------

end


  class LineItem < ApplicationRecord
    has_many :products 
    belongs_to :cart 
    has_many :line_item_products, dependent: :destroy
    has_many :products, through: :line_item_products
  end

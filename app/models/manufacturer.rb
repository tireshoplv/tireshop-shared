class Manufacturer < ApplicationRecord
  include Versionable
  include SetupDeleted
  include SetupVisible
  include GenerateHashId


  # associations
  # ---------------------------------------------------------------
  has_many :product_prices, class_name: 'ProductVendorPrice', dependent: :restrict_with_error

  has_many :aliases, class_name: 'ManufacturerAlias', dependent: :destroy

  accepts_nested_attributes_for :aliases, allow_destroy: true
  has_many :products
  # ---------------------------------------------------------------


  # translations
  # ---------------------------------------------------------------
  translates :name, fallbacks_for_empty_translations: true
  globalize_accessors locales: Settings['locales']['available'], attributes: [:name]
  # ---------------------------------------------------------------


  # scopes
  # ---------------------------------------------------------------
  scope :valid, -> { self.visible.not_deleted }
  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------
  validates :name, translation_presence: { locales: [Settings['locales']['default']] }
  # ---------------------------------------------------------------


  # auto-methods
  # ---------------------------------------------------------------
  after_save :clear_unassigned_aliases
  # ---------------------------------------------------------------


  # class methods
  # ---------------------------------------------------------------

  def self.by_aliases(str)
    result = self.joins(:aliases)
    result = result.where('manufacturer_aliases.name = (?)', str)
    result
  end

  # ---------------------------------------------------------------


  # instance methods
  # ---------------------------------------------------------------

  def admin_option_title
    str = self.name
    str += " (#{I18n.t('activerecord.shared.deleted')})" if self.deleted?
    str += " (#{I18n.t('activerecord.shared.hidden')})"  unless self.visible?
    str
  end

  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------

  def clear_unassigned_aliases
    self.aliases.each do |manufacturer_alias|
      ::ManufacturerUnassignedAlias
        .where(name: manufacturer_alias.name)
        .delete_all
    end
  end

  def set_version
    params = {
      visible: self.visible,
      deleted: self.deleted
    }

    # Name translated fields
    params[:name] = Settings['locales']['available'].map do |loc|
      { loc => self.send("name_#{loc}") }
    end

    params[:aliases] = self.aliases.map do |manufacturer_alias|
      {
        name:         manufacturer_alias.name,
        product_type: manufacturer_alias.product_type
      }
    end

    self.version_body = params
  end

  # ---------------------------------------------------------------


end

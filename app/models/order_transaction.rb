class OrderTransaction < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :order, touch: true
  belongs_to :user, required: false
  # ---------------------------------------------------------------


  # localization
  # ---------------------------------------------------------------
  enumerize :transaction_type,
            in:         I18n.t('enumerize.order_transaction.transaction_type').keys,
            i18n_scope: 'enumerize.order_transaction.transaction_type',
            predicates: { prefix: true }
  # ---------------------------------------------------------------

end

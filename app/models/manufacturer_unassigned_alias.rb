class ManufacturerUnassignedAlias < ApplicationRecord


  # associations
  # ---------------------------------------------------------------
  attr_accessor :manufacturer_id
  # ---------------------------------------------------------------


  # localization
  # ---------------------------------------------------------------
  enumerize :product_type,
            in:         I18n.t('enumerize.product.product_type').keys,
            i18n_scope: 'enumerize.product.product_type',
            predicates: { prefix: true }
  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------
  validates :name, :product_type,
            presence: true

  validates :name,
            uniqueness: { case_sensitive: false }

  after_save :assign_alias
  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------

  def assign_alias
    return if self.manufacturer_id.nil?

    if manufacturer = ::Manufacturer.find_by(id: self.manufacturer_id)
      manufacturer.aliases.create!(name: self.name)
      self.destroy
    end
  end

  # ---------------------------------------------------------------


end

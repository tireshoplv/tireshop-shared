class ProductView < ApplicationRecord
  belongs_to :user, required: false
  belongs_to :product

  validates :product_id, :ip, :user_agent,
            presence: true

end

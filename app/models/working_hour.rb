class WorkingHour < ActiveRecord::Base
  DEFAULT_WORKING_HOURS = {
    monday:    { start: 9, end: 18 },
    tuesday:   { start: 9, end: 18 },
    wednesday: { start: 9, end: 18 },
    thursday:  { start: 9, end: 18 },
    friday:    { start: 9, end: 18 },
    saturday:  { start: 10, end: 16 },
    sunday:    { start: 0, end: 0 }
  }

  DAY_NAME_MAP = {
    monday:    1,
    tuesday:   2,
    wednesday: 3,
    thursday:  4,
    friday:    5,
    saturday:  6,
    sunday:    0
  }

  WORKING_HOURS_REGEX = /\A([0-1]?[0-9]|2[0-3])-[0-5][0-9]\z/
  TIME_FORMATTING_REGEX = /\A([0-1]?[0-9]|2[0-3])[0-5][0-9]\z/

  DEFAULT_START_TIME = "09:00:000"
  DEFAULT_END_TIME = "18:30:000"

  before_validation :format_times

  validates :start_time_monday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_monday.present? && end_time_monday.present? }
  validates :end_time_monday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_monday.present? && end_time_monday.present? }

  validates :start_time_tuesday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_tuesday.present? && end_time_tuesday.present? }
  validates :end_time_tuesday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_tuesday.present? && end_time_tuesday.present? }

  validates :start_time_wednesday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_wednesday.present? && end_time_wednesday.present? }
  validates :end_time_wednesday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_wednesday.present? && end_time_wednesday.present? }

  validates :start_time_thursday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_thursday.present? && end_time_thursday.present? }
  validates :end_time_thursday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_thursday.present? && end_time_thursday.present? }

  validates :start_time_friday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_friday.present? && end_time_friday.present? }
  validates :end_time_friday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_friday.present? && end_time_friday.present? }

  validates :start_time_saturday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_saturday.present? && end_time_saturday.present? }
  validates :end_time_saturday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_saturday.present? && end_time_saturday.present? }

  validates :start_time_sunday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_sunday.present? && end_time_sunday.present? }
  validates :end_time_sunday, format: {with: WORKING_HOURS_REGEX}, if: -> { start_time_sunday.present? && end_time_sunday.present? }

  after_validation :add_zero_padding
  after_validation :validate_time_order

  class << self 
    def initialize_defaults 
      working_hours = first || new
      DEFAULT_WORKING_HOURS.each do |day, hours| 
        unless working_hours.send("start_time_#{day.to_s}").present? 
          working_hours.send("start_time_#{day.to_s}=", default_timestamp(hours[:start]))
        end
  
        unless working_hours.send("end_time_#{day.to_s}").present? 
          working_hours.send("end_time_#{day.to_s}=", default_timestamp(hours[:end]))
        end
      end
      working_hours.save
    end

    def current_working_hours 
      initialize_defaults if count == 0
      first
    end

    def day_name_map
      DAY_NAME_MAP
    end
  
    def week_day_names
      DEFAULT_WORKING_HOURS.keys.map(&:to_s)
    end

    def working_hours 
      working_hours_data = {}

      wh = current_working_hours

      week_day_names.each do |dn| 
        working_hours_data[DAY_NAME_MAP[dn.to_sym]] = {
          start: data_time_value_from_(wh.start_time_for_(dn), :start),
          end: data_time_value_from_(wh.end_time_for_(dn), :end)
        }
      end
      working_hours_data
    end

    def has_minutes?(time)
      time.to_i != time
    end

    def min_time 
      start_times = DAY_NAME_MAP.map { |day, id| self.current_working_hours.start_time_for_(day) }
      start_times.reject! {|time| time == "00-00" }
      start_times.compact!
      min_time_data = start_times.min.split("-")
      return DEFAULT_START_TIME unless min_time_data.present?
      "#{min_time_data.first}:#{min_time_data.last}:00"
      
    rescue 
      DEFAULT_START_TIME
    end

    def max_time 
      end_times = DAY_NAME_MAP.map { |day, id| self.current_working_hours.end_time_for_(day) }
      end_times.reject! {|time| time == "00-00" }
      end_times.compact!
      end_time_data = end_times.max.split("-")
      return DEFAULT_END_TIME unless end_time_data.present?
      "#{end_time_data.first}:#{end_time_data.last}:00"

    rescue 
      DEFAULT_END_TIME
    end

    def working_time_limit_at_(type, date) 
      wh = working_hours[date.wday]
      
      return date unless [:start, :end].include? type
      return if wh[type] == 0

      date.change({
        hour: wh[type].to_i,
        min: WorkingHour.has_minutes?(wh[type]) ? 30 : 0
      })
    end
  end

  def start_time_for_(day)
    self.send("start_time_#{day}")
  rescue => e
    nil
  end

  def end_time_for_(day)
    self.send("end_time_#{day}")
  rescue => e
    nil
  end

  private 

  class << self 
    def default_timestamp(hour)
      return nil if hour == 0
      "#{"%02d" % hour}-00"
    end

    def data_time_value_from_(time, type)
      return 0 unless time.present?
      split = time.split('-').map(&:to_i)
      hours = split.first
      minutes = split.last

      return hours if minutes == 0
      return hours + 1 if minutes > 30 && type == :start
      return hours if minutes < 30 && type == :end
      hours + 0.5
    end    
  end

  def format_times
    self.class.week_day_names.each do |dn| 
      start_time = start_time_for_(dn)
      end_time = end_time_for_(dn)

      self.send("start_time_#{dn}=", format_(start_time))
      self.send("end_time_#{dn}=", format_(end_time))
    end
  end

  def format_(time)
    return unless time
    return time if time.include? '-'
    # return time unless TIME_FORMATTING_REGEX.match? time

    if time.length == 4
      minutes = time.last(2)
      hours = "%02d" % time.chomp(minutes).to_i     
    else 
      minutes = time.last
      hours = "%02d" % time.chomp(minutes).to_i 
      minutes += '0'
    end

    "#{hours}-#{minutes}"
  end

  def padding_format_(time)
    return time if time.length == 5
    time_split = time.split('-')
    hours = "%02d" % time_split.first.to_i
    minutes = "%02d" % time_split.last.to_i if time_split.length != 1 
    minutes ||= '00'
    "#{hours}-#{minutes}"
  end

  def add_zero_padding
    self.class.week_day_names.each do |dn| 
      start_time = start_time_for_(dn)
      end_time = end_time_for_(dn)
      next unless start_time.present? && end_time.present?
      self.send("start_time_#{dn}=", padding_format_(start_time))
      self.send("end_time_#{dn}=", padding_format_(end_time))
    end
  end

  def validate_time_order
    self.class.week_day_names.each do |dn| 
      next unless start_time_for_(dn) && end_time_for_(dn)
      if start_time_for_(dn) > end_time_for_(dn)
        errors.add("start_time_#{dn}".to_sym, :not_valid)
        errors.add("end_time_#{dn}".to_sym, :not_valid)
      end
    end
  end
end

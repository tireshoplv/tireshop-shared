class UserAffiliate < ApplicationRecord
  belongs_to :referred_user, class_name: 'User', primary_key: :id, foreign_key: :referred_user_id # User who signed up
  belongs_to :referee_user,  class_name: 'User', primary_key: :id, foreign_key: :referee_user_id  # User who referred new user
end

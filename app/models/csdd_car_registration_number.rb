class CsddCarRegistrationNumber < ApplicationRecord
  include Versionable
  include SetupDeleted



  # validations - shared
  # ---------------------------------------------------------------

  validates :vehicle_identification_number,
            :registration_number,
            :csdd_updated_at,
            :car_name,
            :car_model,
            :car_year,
            presence: true

  validates :registration_number, uniqueness: { case_sensitive: false }

  validates :car_year,
            length:       { minimum: 4, maximum: 4 },
            numericality: {
              greater_than_or_equal_to: 1903,
              less_than_or_equal_to:    Date.current.year
            }

  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------


  def set_version
    self.version_body = {
      vehicle_identification_number: self.vehicle_identification_number,
      registration_number:           self.registration_number,
      car_name:                      self.car_name,
      car_model:                     self.car_model,
      car_year:                      self.car_year,
      deleted:                       self.deleted
    }
  end


  # ---------------------------------------------------------------


end

=begin

  Product has fields of all product types. This is done for performance,
  cos for large queries and A LOT of products, it became too taxing to
  load extra fields each time.

  Fields for each product type can be seen in Product::FIELDS. This is
  done not only for easy reference, but for any future feature, that
  needs to know what fields are for each product type.

  Product prices are stored in product.product_vendors, cos we have diff
  price for each vendor. So, to update price, we update/create product_vendors.

=end
class Product < ApplicationRecord
  include Versionable
  include SetupDeleted
  include SetupVisible
  include GenerateHashId
  include AssociationReflections::Concern


  CURRENCY_SYMBOL = '€'

  CAR = "passenger_car"
  TRUCK = "truck"
  MOTO = "moto"
  BUS = "bus"
  SUV = "4x4"
  AGRO = "agro"

  WINTER = "w"
  SUMMER = "s"
  MS = "m+s"
  VEHICLE_TYPES = [CAR, TRUCK, MOTO, BUS, SUV, AGRO].freeze


  # image
  # ---------------------------------------------------------------
  mount_uploader :image, ProductImageUploader

  after_save :check_image_resizing

  def check_image_resizing
    if self.image.present?
      self.image.recreate_versions!
    else
      unless self.image_crop.nil?
        self.update_column(:image_crop, nil)
      end
    end
  end
  # ---------------------------------------------------------------


  # Product Fields
  # This should be kept up to date.
  # It is used in:
  #   - toggle show/hide fields in admin panel views
  # ---------------------------------------------------------------
  FIELDS = {
    rim: [
      :ean,
      :url,
      :model,
      :used,
      :image,
      :width,
      :radius,
      :offset,
      :season,
      :diameter,
      :description,
      :centre_bore,
      :freight_class,
      :bolts_included,
      :pitch_circle_diameter
    ],
    tire: [
      :ean,
      :url,
      :used,
      :studs,
      :model,
      :image,
      :usage,
      :width,
      :height,
      :radius,
      :season,
      :diameter,
      :load_index,
      :description,
      :noise_index,
      :noise_level,
      :vehicle_type,
      :speed_rating,
      :road_surface,
      :dedicated_axle,
      :fuel_efficiency,
      :wet_breaking_efficiency
    ],
    battery: [
      :ean,
      :model,
      :image,
      :width,
      :height,
      :length,
      :current,
      :voltage,
      :capacity,
      :description
    ]
  }
  # ---------------------------------------------------------------


  # associations
  # ---------------------------------------------------------------
  belongs_to :manufacturer # TODO: add validation for product_type <-> manufacturer

  has_many :views, class_name: 'ProductView', dependent: :destroy
  has_many :line_item_products, dependent: :destroy
  has_many :line_items, through: :line_item_products
  has_many :product_vendors, dependent: :destroy
  has_many :vendors, through: :product_vendors

  has_many :prices,        class_name: 'ProductVendorPrice', dependent: :destroy # product prices
  has_many :markup_prices, class_name: 'ProductPrice',       dependent: :destroy # product prices generated with markups

  has_many :scraper_inputs, class_name: 'ProductRawScraperInput', dependent: :destroy

  has_many :markup_products, class_name: 'MarkupProduct'

  has_many :cart_historical_products, class_name: 'UserCartHistoricalProduct', dependent: :restrict_with_error
  has_many :cart_product_vendors,     class_name: 'UserCartProductVendor',     dependent: :restrict_with_error

  has_many :product_prices, dependent: :destroy

  accepts_nested_attributes_for :scraper_inputs,  allow_destroy: false
  accepts_nested_attributes_for :product_vendors, allow_destroy: true
  # ---------------------------------------------------------------


  # localization
  # ---------------------------------------------------------------

  enumerize :product_type,
            in:         I18n.t('enumerize.product.product_type').keys,
            i18n_scope: 'enumerize.product.product_type',
            predicates: { prefix: true }

  enumerize :season,
            in:         ::I18n.t('enumerize.product.season').keys,
            i18n_scope: 'enumerize.product.season',
            predicates: { prefix: true }

  enumerize :speed_rating,
            in:         ::I18n.t('enumerize.product.speed_rating').keys,
            i18n_scope: 'enumerize.product.speed_rating',
            predicates: { prefix: true }

  enumerize :fuel_efficiency,
            in:         ::I18n.t('enumerize.product.fuel_efficiency').keys,
            i18n_scope: 'enumerize.product.fuel_efficiency',
            predicates: { prefix: true }

  enumerize :wet_breaking_efficiency,
            in:         ::I18n.t('enumerize.product.wet_breaking_efficiency').keys,
            i18n_scope: 'enumerize.product.wet_breaking_efficiency',
            predicates: { prefix: true }

  # enumerize :vehicle_type,
  #           in:         ::I18n.t('enumerize.product.vehicle_type').keys,
  #           i18n_scope: 'enumerize.product.vehicle_type',
  #           predicates: { prefix: true }

  enumerize :dedicated_axle,
            in:         ::I18n.t('enumerize.product.dedicated_axle').keys,
            i18n_scope: 'enumerize.product.dedicated_axle',
            predicates: { prefix: true }

  enumerize :road_surface,
            in:         ::I18n.t('enumerize.product.road_surface').keys,
            i18n_scope: 'enumerize.product.road_surface',
            predicates: { prefix: true }

  enumerize :noise_level,
            in:         ::I18n.t('enumerize.product.noise_level').keys,
            i18n_scope: 'enumerize.product.noise_level',
            predicates: { prefix: true }

  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------

  validates :manufacturer_id,
            presence: true

  validates :product_vendors,
            length: { minimum: 1, message: ::I18n.t('activerecord.custom_errors.product_vendor_required_at_least_one') }

  validates :uid,
            uniqueness: { scope: :scraper },
            unless:     lambda { |m| m.scraper.nil? }

  validates :ean,
            allow_nil:   true, # this allows nil value
            allow_blank: true, # this prevents nil from counting as uniq value for uniqueness validator
            uniqueness:  true

  validate :validate_ean

  validates :product_type,
            inclusion: { in: I18n.t('enumerize.product.product_type').keys.map(&:to_s) },
            allow_nil: false

  validates :product_type, :uid, :scraper,
            presence: true,
            unless:   lambda { |m| m.scraper.nil? }

  # ---------------------------------------------------------------


  # validations - product_type specific (don't forget to check all scrapers, after changing these.
  #                                      Scrapers skip products if fields bellow are empty)
  # ---------------------------------------------------------------

  validates :model,
            :width,
            :offset,
            :diameter,
            :pitch_circle_diameter,
            presence: true,
            if:       lambda { |m| m.product_type == 'rim' }

  validates :model,
            :width,
            :height,
            :diameter,
            :speed_rating,
            presence: true,
            if:       lambda { |m| m.product_type == 'tire' }

  validates :model,
            :current,
            :voltage,
            :capacity,
            presence: true,
            if:       lambda { |m| m.product_type == 'battery' }

  # ---------------------------------------------------------------


  # Scopes
  # ---------------------------------------------------------------
  scope :tyres,                            ->               { where(product_type: "tire")}
  scope :valid,                            ->                { self.visible.not_deleted }
  scope :by_ean,                           -> (ean)          { where(ean: ean) }
  scope :by_product_type,                  -> (product_type) { where(product_type: product_type) }
  scope :ordered_by_cheapest_vendor_price, -> { joins("LEFT JOIN product_vendor_prices on products.id = product_vendor_prices.product_id").order(ProductVendorPrice.arel_table[:price]) }
  # ---------------------------------------------------------------


  # auto-methods
  # ---------------------------------------------------------------
  after_save :update_product_prices
  # ---------------------------------------------------------------


  # class methods
  # ---------------------------------------------------------------

  def self.products_max_price
    3_000
  end

  def price
    self.prices.first.price
  end

  def self.horizontal_cards(value)
    # fake method that is only used to determinate what card style to show
    self
  end

  def self.universal_search(query)
    ::UniversalSearch::Products.new(query: query, product_association: self).search
  end

  def self.price_search(from, to, markup)
    products = self.joins(:product_prices)

    unless from.to_s.blank?
      products = products.where('product_prices.markup_id = (?) AND product_prices.price >= (?)', markup.id, from.to_f)
    end

    unless to.to_s.blank?
      products = products.where('product_prices.markup_id = (?) AND product_prices.price <= (?)', markup.id, to.to_f)
    end

    products
  end

  # ---------------------------------------------------------------


  # instance methods
  # ---------------------------------------------------------------


  # TODO: find out how this should be displayed properly
  def available_count
    self.product_vendors.pluck(:qty).sum
  end

  def admin_option_title
    str = self.to_s
    str += ' (' + I18n.t('activerecord.shared.invisible') + ')' unless self.visible?
    str += ' (' + I18n.t('activerecord.shared.deleted')   + ')' if self.deleted?
    str
  end


  def to_s
    case self.product_type
      when 'rim'
        "#{self.model} #{self.diameter}' #{self.pitch_circle_diameter} #{self.width} ET#{self.offset}"
      when 'tire'
        "#{self.model} #{self.width}/#{self.height} R#{self.diameter} #{self.speed_rating}"
      when 'battery'
        "#{self.manufacturer.name} #{self.model} #{self.capacity} #{self.current} #{self.voltage}"
    end
  end


  def model_parameters
    case self.product_type
      when 'rim'
        "#{self.diameter}' #{self.pitch_circle_diameter} #{self.width} ET#{self.offset}"
      when 'tire'
        "#{self.width}/#{self.height}/R#{self.diameter} #{self.speed_rating.upcase}"
      when 'battery'
        "#{self.capacity} #{self.current} #{self.voltage}"
    end
  end


  def add_view(ip, user_agent, user_id: nil)
    user = self

    user.views.create(
      ip:         ip,
      user_id:    user_id,
      user_agent: user_agent
    )

    user.view_count      += 1
    user.view_uniq_count = self.views.select(:user_id, :ip, :user_agent).distinct.count
    user.save!
  end


  # TODO: update or remove this
  def to_informative_json
    JSON.parse(self.to_json, symbolize_names: true)
  end


  def update_product_prices
    discounts      = ( Discount.count > 0 ? Discount.all : [nil] )
    markups        = Markup.all
    product_prices = []

    discounts.each do |discount|
      markups.each do |markup|
        self.product_vendors.each do |product_vendor|

          price_calc = ProductPriceCalculator.new(
            markup:         markup,
            discount:       discount,
            product_vendor: product_vendor,
            symbol:         Product::CURRENCY_SYMBOL
          )

          product_prices << {
            markup_id:           markup.id,
            vendor_id:           product_vendor.vendor.id,
            product_id:          self.id,
            manufacturer_id:     self.manufacturer.id,
            product_vendor_id:   product_vendor.id,

            discount_id:         ( discount.nil? ? nil   : discount.id),
            discount_has_coupon: ( discount.nil? ? false : !discount.coupon.nil?),

            original_price:      price_calc.original_price,
            price:               price_calc.price,

            markup_value:        price_calc.markup_value,
            discount_value:      price_calc.discount_value
          }
        end
      end
    end

    self.product_prices.delete_all
    ProductPrice.create!(product_prices)
  end


  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------


  def validate_ean
    unless self.ean.to_s.blank?
      self.errors.add(:ean, I18n.t('activerecord.custom_errors.product_ean_wrong_format')) unless self.ean.length == 13
      self.errors.add(:ean, I18n.t('activerecord.custom_errors.product_ean_wrong_format')) unless self.ean.match(/[0-9]{13}/)
    end
  end


  def self.ransackable_scopes(auth_object = nil)
    %i(universal_search horizontal_cards)
  end


  def set_version
    self.version_body = {
      uid:                     self.uid,
      scraper:                 self.scraper,
      product_type:            self.product_type,
      manufacturer_id:         self.manufacturer_id,
      manufacturer_version_id: self.manufacturer.last_version.id,

      deleted: self.deleted,
      visible: self.visible,

      ean:                     self.ean,
      url:                     self.url,
      used:                    self.used,
      image:                   self.image.url,
      studs:                   self.studs,
      model:                   self.model,
      width:                   self.width,
      height:                  self.height,
      length:                  self.length,
      radius:                  self.radius,
      offset:                  self.offset,
      season:                  self.season,
      current:                 self.current,
      voltage:                 self.voltage,
      capacity:                self.capacity,
      diameter:                self.diameter,
      load_index:              self.load_index,
      centre_bore:             self.centre_bore,
      description:             self.description,
      noise_index:             self.noise_index,
      noise_level:             self.noise_level,
      vehicle_type:            self.vehicle_type,
      speed_rating:            self.speed_rating,
      freight_class:           self.freight_class,
      bolts_included:          self.bolts_included,
      fuel_efficiency:         self.fuel_efficiency,
      pitch_circle_diameter:   self.pitch_circle_diameter,
      wet_breaking_efficiency: self.wet_breaking_efficiency,

      product_vendors: self.product_vendors.map do |product_vendor|
        {
          price:             product_vendor.price,
          qty:               product_vendor.qty,
          qty_hour:          product_vendor.qty_hour,
          qty_days:          product_vendor.qty_days,
          qty_weeks:         product_vendor.qty_weeks,
          vendor_id:         product_vendor.vendor_id,
          vendor_version_id: product_vendor.vendor.last_version.id
        }
      end

    }
  end


  # ---------------------------------------------------------------


end

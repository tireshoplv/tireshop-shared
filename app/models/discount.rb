class Discount < ApplicationRecord
  include Versionable


  # associations
  # ---------------------------------------------------------------
  has_many :vendor_discounts,       class_name: 'DiscountVendor',          dependent: :destroy
  has_many :product_discounts,      class_name: 'DiscountProduct',         dependent: :destroy
  has_many :filtered_products,      class_name: 'DiscountFilteredProduct', dependent: :destroy
  has_many :manufacturer_discounts, class_name: 'DiscountManufacturer',    dependent: :destroy

  accepts_nested_attributes_for :vendor_discounts,       allow_destroy: true
  accepts_nested_attributes_for :product_discounts,      allow_destroy: true
  accepts_nested_attributes_for :filtered_products,      allow_destroy: true
  accepts_nested_attributes_for :manufacturer_discounts, allow_destroy: true
  # ---------------------------------------------------------------


  # translations
  # ---------------------------------------------------------------
  translates :name, fallbacks_for_empty_translations: true
  globalize_accessors locales: Settings['locales']['available'], attributes: [:name]
  # ---------------------------------------------------------------


  # auto-methods
  # ---------------------------------------------------------------
  after_save :update_product_prices
  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------


  def update_product_prices
    Resque.enqueue(ProductPricesUpdaterWorker)
  end


  def set_version
    params = {}

    params[:discount_percent] = self.discount_percent

    params[:vendor_discounts] = self.vendor_discounts do |vendor_discount|
      {
        discount_percent: vendor_discount.discount_percent,
        vendor_id:      vendor_discount.vendor_id
      }
    end

    params[:product_discounts] = self.product_discounts do |product_discount|
      {
        discount_percent: product_discount.discount_percent,
        product_id:     product_discount.product_id
      }
    end

    params[:filtered_products] = self.filtered_products do |filtered_product|
      {
        discount_percent: filtered_product.discount_percent,
        fields:         filtered_product.fields.map do |field|
          {
            field:        field.field,
            product_type: field.product_type
          }
        end
      }
    end

    params[:manufacturer_discounts] = self.manufacturer_discounts do |manufacturer_discount|
      {
        discount_percent:  manufacturer_discount.discount_percent,
        manufacturer_id: manufacturer_discount.manufacturer_id
      }
    end

    self.version_body = params
  end

  # ---------------------------------------------------------------


end

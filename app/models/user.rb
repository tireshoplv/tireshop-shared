class User < ApplicationRecord
  include GenerateHashId
  include SetupDeleted
  include Versionable

  include Users::SetupEmail
  include Users::SetupLocale
  include Users::SetupBlocked
  include Users::SetupPassword
  include Users::SetupPermissions
  include Users::StoreUserParams


  # associations
  # ---------------------------------------------------------------
  has_many :sessions
  has_many :carts
  # has_many :tokens
  # Markup
  # -------------------------------
  belongs_to :markup
  # -------------------------------

  # Cart
  # -------------------------------
  has_many :cart_product_vendors,     class_name: 'UserCartProductVendor',     dependent: :destroy

  has_many :cart_historical_products, class_name: 'UserCartHistoricalProduct', dependent: :destroy
  # -------------------------------

  # Orders & Transactions
  # -------------------------------
  has_many :orders, dependent: :destroy

  has_many :transactions, class_name: 'OrderTransaction'
  # -------------------------------

  # Affiliates
  # -------------------------------
  has_many :affiliated_referred_users, class_name: 'UserAffiliate', dependent: :destroy, foreign_key: :id, primary_key: :referred_user_id
  has_one  :affiliated_referee_user,   class_name: 'UserAffiliate', dependent: :destroy, foreign_key: :id, primary_key: :referee_user_id

  has_many :referred_users, class_name: 'User', through: :affiliated_referred_users
  has_one  :referee_user,   class_name: 'User', through: :affiliated_referee_user
  # -------------------------------

  # ---------------------------------------------------------------


  # scopes
  # ---------------------------------------------------------------
  scope :valid, -> { self.not_blocked.not_deleted }
  scope :mocked_store_user, -> { where(email: "veikals@tireshop.lv").first_or_create!(STORE_USER_PARAMS) }
  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------
  validates :markup_id, :role, :locale,
            presence: true
  # ---------------------------------------------------------------


  # instance methods
  # ---------------------------------------------------------------


  def purchase_cart_products
    ::PaymentSystem.new(self, payment_method: 'bank').purchase_cart_products
  end


  def generate_total_cart_price(markup: nil)
    prices = self.cart_product_vendors.map do |cart_product_vendor|
      cart_product_vendor.product_vendor.generate_price(markup: markup, count: cart_product_vendor.count).price
    end

    if prices.any?
      Product::CURRENCY_SYMBOL + '%.2f' %prices.sum.to_f
    end
  end


  def admin_option_title
    str = self.email
    str += " (#{I18n.t('activerecord.shared.deleted')})" if self.deleted?
    str += " (#{I18n.t('activerecord.shared.blocked')})" if self.blocked?
    str
  end

  def active_cart 
    self.carts.where(assm_state: "active").first
  end


  def add_product_vendor_to_cart(product_vendor)
    params = { product_vendor_id: product_vendor.id, product_id: product_vendor.product.id }

    if self.cart_product_vendors.where(params).count.zero?
      self.cart_product_vendors.create!(params)
    end
  end


  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------

  def set_version
    self.version_body = {
      role:               self.role,
      email:              self.email,
      locale:             self.locale,
      deleted:            self.deleted,
      blocked:            self.blocked,
      blocked_reason:     self.blocked_reason,
      encrypted_password: self.encrypted_password
    }
  end

  # ---------------------------------------------------------------


end

class ServicePost < ApplicationRecord
  include GenerateHashId
  include SetupDeleted
  include SetupVisible
  include Versionable

  mount_uploader :image, ServicePostUploader


  # validations
  # ---------------------------------------------------------------
  validates :title, :body, :image,
            presence: true
  # ---------------------------------------------------------------


  # translations
  # ---------------------------------------------------------------
  translates :title, :body, fallbacks_for_empty_translations: true
  globalize_accessors locales: Settings['locales']['available'], attributes: [:title, :body]
  # ---------------------------------------------------------------


  # scopes
  # ---------------------------------------------------------------
  scope :valid, -> { self.visible.not_deleted }
  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------

  def set_version
    self.version_body = {
      body:    self.body,
      title:   self.title,
      visible: self.visible,
      deleted: self.deleted
    }
  end


  # ---------------------------------------------------------------

end

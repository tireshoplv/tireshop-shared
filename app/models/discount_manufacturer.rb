class DiscountManufacturer < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :discount, touch: true
  belongs_to :manufacturer, required: false
  # ---------------------------------------------------------------

end

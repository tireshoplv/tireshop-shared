class DiscountProduct < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :discount, touch: true
  belongs_to :product, required: false
  # ---------------------------------------------------------------

end

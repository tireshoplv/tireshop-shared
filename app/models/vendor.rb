class Vendor < ApplicationRecord
  include GenerateHashId
  include SetupDeleted
  include SetupVisible
  include Versionable


  # associations
  # ---------------------------------------------------------------
  has_many :product_vendors, dependent: :restrict_with_error
  has_many :products, through: :product_vendors

  has_many :order_other_items, dependent: :destroy

  has_many :product_prices, class_name: 'ProductVendorPrice', dependent: :restrict_with_error

  has_many :aliases, class_name: 'VendorAlias', dependent: :destroy

  accepts_nested_attributes_for :aliases,         allow_destroy: true
  accepts_nested_attributes_for :product_vendors, allow_destroy: true
  # ---------------------------------------------------------------


  # translations
  # ---------------------------------------------------------------
  translates :name, fallbacks_for_empty_translations: true
  globalize_accessors locales: Settings['locales']['available'], attributes: [:name]
  # ---------------------------------------------------------------


  # scopes
  # ---------------------------------------------------------------
  scope :valid, -> { self.visible.not_deleted }
  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------
  validates :name, translation_presence: { locales: [Settings['locales']['default']] }
  # ---------------------------------------------------------------


  # auto-methods
  # ---------------------------------------------------------------
  after_save :clear_unassigned_aliases
  # ---------------------------------------------------------------


  # class methods
  # ---------------------------------------------------------------

  def self.find_by_alias(str)
    self.joins(:aliases).where('vendor_aliases.name = (?)', str).first
  end

  # ---------------------------------------------------------------


  # instance methods
  # ---------------------------------------------------------------

  def admin_option_title
    str = self.name
    str += " (#{I18n.t('activerecord.shared.deleted')})" if self.deleted?
    str += " (#{I18n.t('activerecord.shared.hidden')})"  unless self.visible?
    str
  end

  def to_s
    self.name
  end

  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------

  def clear_unassigned_aliases
    ::VendorUnassignedAlias.where(name: self.aliases.pluck(:name)).delete_all
  end

  def set_version
    params = {
      add_pvn: self.add_pvn,
      visible: self.visible,
      deleted: self.deleted
    }

    # Name translated fields
    params[:name] = Settings['locales']['available'].map do |loc|
      { loc => self.send("name_#{loc}") }
    end

    self.version_body = params
  end

  # ---------------------------------------------------------------


end

class Contact < ApplicationRecord
  validates_presence_of :image, :first_name, :last_name, :title, :email, :phone
  validates_uniqueness_of :email, :phone
end

# Usage in model:
#
# validates :title, translation_presence: true,
# validates :title, translation_presence: { message: '', locales: [:lv] } # validate specific locales
#
class TranslationPresenceValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    locales = @options[:locales].any? ? @options[:locales].map(&:to_s) : I18n.available_locales.map(&:to_s)

    locales.each do |locale|
      if record.send("#{attribute}_#{locale}").blank?
        record.errors.add("#{attribute}_#{locale}", options[:message] || :blank)
      end
    end
  end

end

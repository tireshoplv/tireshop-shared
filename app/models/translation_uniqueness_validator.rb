# Usage in model:
#
# validates :title, translation_uniqueness: true # will assume it must be uniq across all languages
# validates :title, translation_uniqueness: {scope: :locale, message: ''}
#
class TranslationUniquenessValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    if record.translations.any?
      c  = record.class.to_s.underscore.singularize
      ct = "#{c}_translations"
      id = "#{c}_id"

      records = record.class.joins(:translations)

      unless record.id.nil?
        records = records.where("#{ct}.#{id} != (?)", record.id)
      end

      record.translations.each do |t|
        if options[:scope] == :locale
          records = records.where("#{ct}.locale = '#{t.locale.to_s}' AND #{ct}.#{attribute} = '#{t[attribute]}'")
        else
          records = records.where("#{ct}.#{attribute} = '#{t[attribute]}'")
        end


        if records.any?
          record.errors.add("#{attribute}_#{t.locale}", options[:message] || :uniqueness)
        end
      end
    end
  end

end

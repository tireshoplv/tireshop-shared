class MarkupProduct < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :markup, touch: true
  belongs_to :product, required: false
  # ---------------------------------------------------------------

end

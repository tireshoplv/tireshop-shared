class PdfDescription < ApplicationRecord

  # translations
  # ---------------------------------------------------------------
  translates :order_desc, fallbacks_for_empty_translations: true
  globalize_accessors locales: Settings['locales']['available'], attributes: [:order_desc]
  # ---------------------------------------------------------------

end

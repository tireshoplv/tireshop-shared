class Markup < ApplicationRecord
  include Versionable


  # associations
  # ---------------------------------------------------------------
  has_many :users
  has_many :vendor_markups,       class_name: 'MarkupVendor',          dependent: :destroy
  has_many :product_markups,      class_name: 'MarkupProduct',         dependent: :destroy
  has_many :filtered_products,    class_name: 'MarkupFilteredProduct', dependent: :destroy
  has_many :manufacturer_markups, class_name: 'MarkupManufacturer',    dependent: :destroy

  accepts_nested_attributes_for :vendor_markups,       allow_destroy: true
  accepts_nested_attributes_for :product_markups,      allow_destroy: true
  accepts_nested_attributes_for :filtered_products,    allow_destroy: true
  accepts_nested_attributes_for :manufacturer_markups, allow_destroy: true
  # ---------------------------------------------------------------


  # translations
  # ---------------------------------------------------------------
  translates :name, fallbacks_for_empty_translations: true
  globalize_accessors locales: Settings['locales']['available'], attributes: [:name]
  # ---------------------------------------------------------------


  # auto-methods
  # ---------------------------------------------------------------
  after_save :update_product_prices
  # ---------------------------------------------------------------


  # Default markup methods (Default markup cannot be deleted)
  # ---------------------------------------------------------------
  def self.default
    Markup.find(Markup.default_id)
  end

  def self.default_id
    1
  end

  def default?
    self.id == Markup.default_id
  end
  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------


  def update_product_prices
    Resque.enqueue(ProductPricesUpdaterWorker)
  end


  def set_version
    params = {}

    params[:markup_percent] = self.markup_percent

    params[:vendor_markups] = self.vendor_markups do |vendor_markup|
      {
        markup_percent: vendor_markup.markup_percent,
        vendor_id:      vendor_markup.vendor_id
      }
    end

    params[:product_markups] = self.product_markups do |product_markup|
      {
        markup_percent: product_markup.markup_percent,
        product_id:     product_markup.product_id
      }
    end

    params[:filtered_products] = self.filtered_products do |filtered_product|
      {
        markup_percent: filtered_product.markup_percent,
        fields:         filtered_product.fields.map do |field|
          {
            field:        field.field,
            product_type: field.product_type
          }
        end
      }
    end

    params[:manufacturer_markups] = self.manufacturer_markups do |manufacturer_markup|
      {
        markup_percent:  manufacturer_markup.markup_percent,
        manufacturer_id: manufacturer_markup.manufacturer_id
      }
    end

    self.version_body = params
  end

  # ---------------------------------------------------------------


end

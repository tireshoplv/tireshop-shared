class ManufacturerAlias < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :manufacturer, touch: true
  # ---------------------------------------------------------------

  # gloablize
  # ---------------------------------------------------------------
  enumerize :product_type,
            in:         I18n.t('enumerize.product.product_type').keys,
            i18n_scope: 'enumerize.product_type.product_type',
            predicates: { prefix: true }
  # ---------------------------------------------------------------

  # validations
  # ---------------------------------------------------------------
  validates :name,
            presence: true
  # ---------------------------------------------------------------

  # auto-methods
  # ---------------------------------------------------------------
  before_save :downcase_name
  # ---------------------------------------------------------------

  private

  # ---------------------------------------------------------------

  def downcase_name
    self.name = self.name.to_s.downcase
  end

  # ---------------------------------------------------------------

end

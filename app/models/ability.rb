# frozen_string_literal: true

class Ability
  include ::CanCan::Ability

  def initialize(current_user, layout: 'application')
    shared(current_user)
    admin(current_user)       if layout == 'admin'
    application(current_user) if layout == 'application'
  end

  def shared(current_user)
    # Developer
    # -----------------------------------------------------------------------------
    if current_user.try(:role) == 'developer'
      can %i[read create], Order
      can [:update],                    Order, { paid: false }
      can [:destroy],                   Order
      can [:send_offer],                Order
      can [:create_user],               Order
      can [:view_versions],             Order
      can [:mark_as_paid],              Order.where(paid: false).where.not(price: nil)
      can %i[refund refund_to_wallet], Order, { paid: true }

      can :update_order_fields, %i[
        order_type status
        user_id custom_legal_fields
        reg_nr fax_nr pvn_reg_nr phone_nr mobile_phone_nr company_name
        address city post_code bank_nr bank_swift bank_iban
        payment_method delivery_method
        assembly assembly_price
        admin_notes user_notes
      ]

      can :update_order_product_vendors_fields, %i[product_vendor_id price count id _destroy]

      can :update_order_other_items_fields, %i[name vendor_id price count id _destroy]

    end
    # -----------------------------------------------------------------------------

    # User
    # -----------------------------------------------------------------------------
    if current_user.try(:role) == 'user'
    end
    # -----------------------------------------------------------------------------

    # Guest
    # -----------------------------------------------------------------------------
    unless current_user
    end
    # -----------------------------------------------------------------------------
  end

  def application(current_user)
    # Developer
    # -----------------------------------------------------------------------------
    if current_user.try(:role) == 'developer'

    end
    # -----------------------------------------------------------------------------

    # User
    # -----------------------------------------------------------------------------
    if current_user.try(:role) == 'user'
    end
    # -----------------------------------------------------------------------------

    # Guest
    # -----------------------------------------------------------------------------
    unless current_user
    end
    # -----------------------------------------------------------------------------
  end

  def admin(current_user)
    # Developer
    # -----------------------------------------------------------------------------
    if current_user.try(:role) == 'developer'

      # Ajax requests
      # ---------------------------------
      can [:select_products], Admin::AjaxRequestsController
      # ---------------------------------

      # Admin Pages
      # ---------------------------------
      can [:dashboard], Admin::PagesController
      # ---------------------------------

      # Versions
      # ---------------------------------
      can [:read], Version
      can %i[show index], Admin::VersionsController
      # ---------------------------------

      # Discounts
      # ---------------------------------
      can %i[read create update], Discount
      can %i[delete un_delete], Discount

      can [:update_discount_vendors],           Discount
      can [:update_discount_products],          Discount
      can [:update_discount_manufacturers],     Discount
      can [:update_discount_filtered_products], Discount

      can :update_discount_fields, %i[discount_percent coupon]

      can :update_discount_translated_fields, [:name]

      can %i[index show edit new create edit update delete], Admin::DiscountsController
      # ---------------------------------

      # Markups
      # ---------------------------------
      can %i[read create update], Markup
      can %i[delete un_delete], Markup.where.not(id: Markup.default_id) # default markup cannot be deleted

      can [:update_markup_vendors],           Markup
      can [:update_markup_products],          Markup
      can [:update_markup_manufacturers],     Markup
      can [:update_markup_filtered_products], Markup

      can :update_markup_fields, [:markup_percent]

      can :update_markup_translated_fields, [:name]

      can %i[index show edit new create edit update delete], Admin::MarkupsController
      # ---------------------------------

      # Orders
      # ---------------------------------
      can %i[read create], Order
      can [:update],                    Order, { paid: false }
      can [:destroy],                   Order
      can [:send_offer],                Order
      can [:create_user],               Order
      can [:view_versions],             Order
      can [:mark_as_paid],              Order.where(paid: false).where.not(price: nil)
      can %i[refund refund_to_wallet], Order, { paid: true }

      can :update_order_fields, %i[
        order_type status
        user_id custom_legal_fields
        reg_nr fax_nr pvn_reg_nr phone_nr mobile_phone_nr company_name
        address city post_code bank_nr bank_swift bank_iban
        payment_method delivery_method
        assembly assembly_price
        admin_notes user_notes
      ]

      can :update_order_product_vendors_fields, %i[product_vendor_id price count id _destroy]

      can :update_order_other_items_fields, %i[name vendor_id price count id _destroy]

      can %i[
        index show new create edit update destroy
        quick_edit quick_update
        user_new user_create
        refund refund_to_wallet mark_as_paid send_offer
        toggle_product_in_cart preview pdf
        product_vendor_selected user_selected
      ], Admin::OrdersController
      # ---------------------------------

      # PDF Descriptions
      # ---------------------------------
      can %i[read update], PdfDescription

      can :update_pdf_description_translated_fields, [
        :order_desc
      ]

      can %i[edit update], Admin::Orders::PdfDescriptionsController
      # ---------------------------------

      # Users
      # ---------------------------------
      can %i[read update create], User
      can %i[delete un_delete], User
      can %i[block un_block], User
      can [:destroy],                User
      can [:view_versions],          User
      can :login_as,                 User.where.not(id: current_user.id)
      can :change_user_role,         %i[developer user]
      can :update_user_fields,       %i[email role locale password markup_id]

      can %i[index show edit update new create delete destroy], Admin::UsersController
      # ---------------------------------

      # Contacts
      
      can %i[index show edit new create edit update delete], Admin::ContactsController

      # Products
      # ---------------------------------
      can %i[read update create], Product
      can %i[delete un_delete],             Product
      can %i[hide show],                    Product
      can [:destroy],                        Product
      can [:view_versions],                  Product
      can [:view_price_history],             Product
      can [:update_product_product_vendors], Product
      can :update_product_fields, %i[
        manufacturer_id product_type
        ean
        url
        used
        usage
        model
        studs
        width
        height
        length
        offset
        radius
        season
        current
        voltage
        capacity
        diameter
        load_index
        noise_index
        noise_level
        centre_bore
        description
        vehicle_type
        speed_rating
        road_surface
        freight_class
        dedicated_axle
        bolts_included
        fuel_efficiency
        pitch_circle_diameter
        wet_breaking_efficiency
        image
        image_crop
        remove_image
      ]

      # can %i[index show edit update new create delete destroy store_search show_invalid_products delete_invalid_products], Admin::ProductsController
      # ---------------------------------

      # Vendors
      # ---------------------------------
      can %i[read update create],              Vendor
      can %i[delete un_delete],                  Vendor
      can %i[hide show],                         Vendor
      can [:destroy],                             Vendor
      can [:view_versions],                       Vendor
      can :update_vendor_aliases,           Vendor
      can :update_vendor_fields,            %i[add_pvn product_type]
      can :update_vendor_translated_fields, [:name]

      can %i[index show edit update new create delete destroy], Admin::VendorsController
      # ---------------------------------

      # Vendors Unassigned Alias
      # ---------------------------------
      can %i[read update], VendorUnassignedAlias
      can [:destroy], Manufacturer
      can %i[index edit update destroy], Admin::Vendors::UnassignedAliasesController
      # ---------------------------------

      # Manufacturers
      # ---------------------------------
      can %i[read update create], Manufacturer
      can %i[delete un_delete],                  Manufacturer
      can %i[hide show],                         Manufacturer
      can [:destroy],                             Manufacturer
      can [:view_versions],                       Manufacturer
      can :update_manufacturer_aliases,           Manufacturer
      can :update_manufacturer_fields,            [:product_type]
      can :update_manufacturer_translated_fields, [:name]

      can %i[index show edit update new create delete destroy], Admin::ManufacturersController
      # ---------------------------------

      # Manufacturers Unassigned Alias
      # ---------------------------------
      can %i[read update destroy], ManufacturerUnassignedAlias
      can %i[index edit update destroy], Admin::Manufacturers::UnassignedAliasesController
      # ---------------------------------

      # Scrapers
      # ---------------------------------
      can %i[index start start_foreground stop], Admin::ScrapersController
      # ---------------------------------

      # Scraper Errors
      # ---------------------------------
      can %i[read destroy], ScraperError
      can %i[index show destroy destroy_all], Admin::Scrapers::ErrorsController
      # ---------------------------------

      # Scraper Invalid Records
      # ---------------------------------
      can %i[read destroy], ScraperInvalidRecord
      can %i[index show destroy destroy_all], Admin::Scrapers::InvalidRecordsController
      # ---------------------------------

      # Services
      # ---------------------------------
      can %i[read update create], ServicePost
      can %i[delete un_delete], ServicePost
      can [:destroy],                  ServicePost
      can [:view_versions],            ServicePost
      can :update_service_post_fields, %i[title image remove_image body]

      can %i[index show edit update new create delete destroy], Admin::Pages::ServicePostsController
      # ---------------------------------

      # Services
      # ---------------------------------
      can %i[read update create], CsddCarRegistrationNumber
      can %i[delete un_delete], CsddCarRegistrationNumber
      can [:destroy],                                  CsddCarRegistrationNumber
      can [:view_versions],                            CsddCarRegistrationNumber
      can :update_csdd_car_registration_number_fields, %i[
        vehicle_identification_number
        registration_number
        csdd_updated_at
        car_model
        car_name
        car_year
      ]

      can %i[index show edit update new create delete destroy import_from_excel],
          Admin::CsddCarRegistrationNumbersController
      # ---------------------------------

    end
    # -----------------------------------------------------------------------------

    # User
    # -----------------------------------------------------------------------------
    if current_user.try(:role) == 'user'
    end
    # -----------------------------------------------------------------------------

    # Guest
    # -----------------------------------------------------------------------------
    unless current_user
    end
    # -----------------------------------------------------------------------------
  end
end

class OrderProductVendor < ApplicationRecord

  # associations
  # ---------------------------------------------------------------
  belongs_to :order, touch: true

  belongs_to :product
  belongs_to :product_vendor

  belongs_to :markup_version,  class_name: 'Version', foreign_key: :markup_version_id
  belongs_to :product_version, class_name: 'Version', foreign_key: :product_version_id
  # ---------------------------------------------------------------


  # auto-methods
  # ---------------------------------------------------------------
  before_validation :set_associations_and_versions
  # ---------------------------------------------------------------


  # validations
  # ---------------------------------------------------------------

  validates :price,
            numericality: { greater_than: 0 },
            presence:     true

  validates :count,
            numericality: { greater_than: 0 }

  # ---------------------------------------------------------------


  private


  # ---------------------------------------------------------------

  def set_associations_and_versions
    return unless self.id.nil?
    self.product_id         = self.product_vendor.product_id
    self.product_version_id = self.product_vendor.product.last_version.id
    self.markup_version_id  = self.order.user.markup.last_version.id

    if self.price == 0.0 || self.price.nil?
      self.price = self.product_vendor.generate_price(markup: self.order.user.markup).price
    end
  end

  # ---------------------------------------------------------------


end
